package vassal.analysis.voronoi.java;

import java.util.*;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.Test;
import static org.junit.Assert.*;
import vassal.analysis.PairDistanceAnalysis;
import vassal.data.Atom;
import vassal.data.Cell;
import vassal.data.AtomImage;

/**
 *
 * @author Logan Ward
 */
public class VoronoiFaceTest {

    @Test
    public void testInitialize() throws Exception {
        // Make a simple crystal
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 0));

        // Initialize faces
        AtomImage image = new AtomImage(cell.getAtom(0), new int[]{0,0,1});
        VoronoiFace face = new VoronoiFace(cell.getAtom(0), image, true);

        // Check out properties
        assertEquals(new Vector3D(0, 0, 0.5), face.getFaceCenter());
        assertEquals(0.5, face.getFaceDistance(), 1e-6);
        assertEquals(new Vector3D(0,0,1), face.getPlane().getNormal());
    }

    @Test
    public void testSimpleAssemble() throws Exception {
        // Make a simple crystal
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 0));

        // Create face to be assemebled
        AtomImage image = new AtomImage(cell.getAtom(0), new int[]{0,0,1});
        VoronoiFace faceToAssemble = new VoronoiFace(cell.getAtom(0), image, true);

        // Initialize faces
        List<VoronoiFace> faces = new ArrayList(5);
        faces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(0), new int[]{-1,0,0}), true));
        faces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(0), new int[]{1,0,0}), true));
        faces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(0), new int[]{0,-1,0}), true));
        faces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(0), new int[]{0,1,0}), true));

        // Attempt to assemble face
        assertTrue(faceToAssemble.assembleFaceFromFaces(faces));

        // Check out properties
        assertEquals(4, faceToAssemble.NEdges());
        assertEquals(1.0, faceToAssemble.getArea(), 1e-6);
    }

    @Test
    public void testFCCSimpleAssemble() throws Exception {
        // Make a fcc crystal
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 0));
        cell.addAtom(new Atom(new double[]{0.5,0.5,0.0}, 0));
        cell.addAtom(new Atom(new double[]{0.5,0.0,0.5}, 0));
        cell.addAtom(new Atom(new double[]{0.0,0.5,0.5}, 0));

        // Create face to be assemebled
        AtomImage image = new AtomImage(cell.getAtom(1), new int[]{0,0,0});
        VoronoiFace faceToAssemble = new VoronoiFace(cell.getAtom(0), image, true);

        // Initialize faces that will be its neighbors
        List<VoronoiFace> neighborFaces = new LinkedList<>();
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(2), new int[]{0,0,0}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(2), new int[]{0,0,-1}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(3), new int[]{0,0,0}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(3), new int[]{0,0,-1}),
                true));

        // Attempt to assemble face
        assertTrue(faceToAssemble.assembleFaceFromFaces(neighborFaces));

        // Check out properties
        assertEquals(4, faceToAssemble.NEdges());

        // Now, attempt to assemble face considering all possible neighbors
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(1), new int[]{0,0,0}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(1), new int[]{0,-1,0}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(1), new int[]{-1,0,0}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(1), new int[]{-1,-1,0}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(2), new int[]{-1,0,0}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(2), new int[]{-1,0,-1}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(3), new int[]{0,-1,0}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(3), new int[]{0,-1,-1}),
                true));
        faceToAssemble = new VoronoiFace(cell.getAtom(0), image, true);

        // Attempt to assemble face
        assertTrue(faceToAssemble.assembleFaceFromFaces(neighborFaces));

        // Check out properties
        assertEquals(4, faceToAssemble.NEdges());
    }

    @Test
    public void testFCCDirectAssemble() throws Exception {
        // Make a fcc crystal
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 0));
        cell.addAtom(new Atom(new double[]{0.5,0.5,0.0}, 0));
        cell.addAtom(new Atom(new double[]{0.5,0.0,0.5}, 0));
        cell.addAtom(new Atom(new double[]{0.0,0.5,0.5}, 0));

        // Create face to be assemebled
        AtomImage image = new AtomImage(cell.getAtom(1), new int[]{0,0,0});
        VoronoiFace faceToAssemble = new VoronoiFace(cell.getAtom(0), image, true);

        // Initialize faces that direct neighbors of atom 0
        List<VoronoiFace> neighborFaces = new LinkedList<>();
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(1), new int[]{0,0,0}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(1), new int[]{0,-1,0}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(1), new int[]{-1,0,0}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(1), new int[]{-1,-1,0}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(2), new int[]{-1,0,0}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(2), new int[]{-1,0,-1}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(2), new int[]{0,0,0}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(2), new int[]{0,0,-1}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(3), new int[]{0,0,0}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(3), new int[]{0,0,-1}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(3), new int[]{0,-1,0}),
                true));
        neighborFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(3), new int[]{0,-1,-1}),
                true));

        // Attempt to assemble face
        assertTrue(faceToAssemble.assembleFaceFromFaces(neighborFaces));

        // Check out properties
        assertEquals(4, faceToAssemble.NEdges());

        // Now, see if all of the faces assemble
        double volume = 0.0;
        for (VoronoiFace face : neighborFaces) {
            assertTrue(face.assembleFaceFromFaces(neighborFaces));
            assertEquals(4, face.NEdges());
            volume += face.getArea() * face.getFaceDistance() / 3;
        }
        assertEquals(0.25, volume, 1e-6);
    }

    @Test
    public void testFCCFullAssemble() throws Exception {
        // Make a fcc crystal
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 0));
        cell.addAtom(new Atom(new double[]{0.5,0.5,0.0}, 0));
        cell.addAtom(new Atom(new double[]{0.5,0.0,0.5}, 0));
        cell.addAtom(new Atom(new double[]{0.0,0.5,0.5}, 0));

        // Create face to be assemebled
        AtomImage image = new AtomImage(cell.getAtom(1), new int[]{0,0,0});
        VoronoiFace faceToAssemble = new VoronoiFace(cell.getAtom(0), image, true);

        // Get neighbor finding tool
        PairDistanceAnalysis imageFinder = new PairDistanceAnalysis();
        imageFinder.setCutoffDistance(3.0);
        imageFinder.analyzeStructure(cell);

        // Initialize any face
        List<VoronoiFace> neighborFaces = new LinkedList<>();
        for (Pair<AtomImage,Double> newImage :
                imageFinder.getAllNeighborsOfAtom(0)) {
            try {
                VoronoiFace face = new VoronoiFace(cell.getAtom(0), newImage.getKey(), true);
                neighborFaces.add(face);
            } catch (Exception e) {
                // Do nothing
            }
        }

        // Attempt to assemble face
        assertTrue(faceToAssemble.assembleFaceFromFaces(neighborFaces));

        // Check out properties
        assertEquals(4, faceToAssemble.NEdges());
    }

    @Test
    public void testIntersections() throws Exception {
        // Make a fcc crystal
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 0));

        // Create test face
        AtomImage image = new AtomImage(cell.getAtom(0), new int[]{0,0,2});
        VoronoiFace testFace = new VoronoiFace(cell.getAtom(0), image, true);
        List<VoronoiFace> originalFaces = new ArrayList(4);
        originalFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(0), new int[]{-1,0,0}), true));
        originalFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(0), new int[]{2,0,0}), true));
        originalFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(0), new int[]{0,-1,0}), true));
        originalFaces.add(new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(0), new int[]{0,1,0}), true));
        testFace.assembleFaceFromFaces(originalFaces);
        assertEquals(1.5, testFace.getArea(), 1e-6);

        // Ensure that a non-intersecting face returns null
        assertEquals(null, testFace.computeIntersection(new VoronoiFace(
                cell.getAtom(0),
                new AtomImage(cell.getAtom(0), new int[]{3,0,0}), true)));

        // Check intersection with a face that cuts of an edge completely
        VoronoiFace newFace = new VoronoiFace(
                cell.getAtom(0),
                new AtomImage(cell.getAtom(0), new int[]{1,0,0}), true);
        assertEquals(1.0, testFace.getCutLength(newFace), 1e-6);
        VoronoiEdge newEdge = testFace.computeIntersection(newFace);
        assertTrue(newEdge != null);
        assertEquals(newFace, newEdge.getEdgeFace());
        assertEquals(testFace, newEdge.getIntersectingFace());
        assertTrue(testFace.isClosed());
        assertEquals(1.0, testFace.getArea(), 1e-6);

        // Compute intersection with a face that adds a new edge
        testFace.assembleFaceFromFaces(originalFaces);
        newFace = new VoronoiFace(cell.getAtom(0),
                new AtomImage(cell.getAtom(0), new int[]{1,1,0}), true);
        assertEquals(Math.sqrt(2)/2, testFace.getCutLength(newFace), 1e-6);
        newEdge = testFace.computeIntersection(newFace);
        assertTrue(newEdge != null);
        assertEquals(newFace, newEdge.getEdgeFace());
        assertEquals(testFace, newEdge.getIntersectingFace());
        assertEquals(5, testFace.NEdges());
    }
}
