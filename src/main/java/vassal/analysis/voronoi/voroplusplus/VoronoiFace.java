package vassal.analysis.voronoi.voroplusplus;

import java.util.List;
import vassal.analysis.voronoi.BaseVoronoiFace;
import vassal.data.Atom;
import vassal.data.AtomImage;

/**
 * Implementation of Voronoi face for Voro++
 * @author Logan Ward
 */
public class VoronoiFace extends BaseVoronoiFace {

    /**
     * Create a blank Voronoi face
     * @param insideAtom Atom inside of the face
     * @param outsideAtom Image of atom outside face
     * @param radical Whether this face is in a radical plane tessellation
     */
    public VoronoiFace(Atom insideAtom, AtomImage outsideAtom, boolean radical) {
        super(insideAtom, outsideAtom, radical);
    }

    /**
     * Assemble a face from a list of vertices.
     * @param vertices Vertices for this face, in sequential order
     */
    public void assembleFromVertices(List<VoronoiVertex> vertices) {
        // Store the vertices
        Vertices.clear();
        Vertices.addAll(vertices);

        // Create the edges
        Edges.clear();
        for (int v=0; v<Vertices.size(); v++) {
            VoronoiEdge edge = new VoronoiEdge(Vertices.get(v),
                    Vertices.get((v+1) % Vertices.size()), this);
            Edges.add(edge);
        }
    }

    @Override
    public boolean isClosed() {
        return Vertices.size() >= 3;
    }
}
