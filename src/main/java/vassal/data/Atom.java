package vassal.data;

/**
 * Represents a single atom.
 * @author Logan Ward
 */
public class Atom implements java.io.Serializable, java.lang.Cloneable {
	/** Position in fractional coordinates */
	private double[] Position;
    /** Position in Cartesian coordinates */
    private double[] PositionCartesian;
	/** Type of atom */
	private int Type;
	/** Cell associated with this atom */
	private Cell Cell;
	/** ID number of this atom */
	private int ID;
	/** Radius of atom */
	private double Radius = 1.0;

	/**
	 * Create a new atom
	 * @param position Position in fractional coordinates
	 * @param type Type (index)
	 */
	public Atom(double[] position, int type) {
		this.Position = position;
		this.Type = type;
	}

    @Override
    public Atom clone() {
        Atom x;
        try {
            x = (Atom) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error(e);
        }
        x.Position = Position.clone();
        x.PositionCartesian = PositionCartesian.clone();
        return x;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Atom ) {
            Atom other = (Atom) obj;

            // Compare type
            String myName = getTypeName();
            String yourName = other.getTypeName();
            if (! (myName.equals("NA") && yourName.equals("NA"))) {
                // If type has name, compare names
                if (! myName.equals(other.getTypeName()))
                    return false;
            } else {
                // Otherwise, compare index
                if (Type != other.Type) {
                    return false;
                }
            }

            // Compare positions: Equal if they are closer than 0.001 cartesian
            double[] myPos = getPositionCartesian();
            double[] otherPos = other.getPositionCartesian();
            double dist = 0;
            for (int i=0; i<3; i++) {
                double temp = myPos[i] - otherPos[i];
                dist += temp * temp;
                if (dist > 1e-6) { // (0.001) ^ 2
                    return false;
                }
            }
            return true;
        }
        return false;
    }



	/**
	 * Set the ID number of this atom
	 * @param ID Desired ID
	 */
	public void setID(int ID) {
		this.ID = ID;
	}

    /**
     * Set type of atom
     * @param type Index of type
     */
    public void setType(int type) {
        this.Type = type;
        if (Cell != null) {
            while (Cell.nTypes() <= Type) {
                Cell.addType(null);
            }
        }
    }

	/**
	 * Define the cell in which this atom is situated
	 * @param cell Cell
	 */
	public void setCell(Cell cell) {
		this.Cell = cell;
        updateCartesianCoordinates();
	}

    /**
     * Recompute the Cartesian coordinates of this atom.
     * @see Cell#setBasis(double[][])
     */
    public void updateCartesianCoordinates() {
        PositionCartesian = Cell.convertFractionalToCartestian(Position);
    }

	/**
	 * Get cell that contains this atom
	 * @return Link to the cell
	 */
	public Cell getCell() {
		return Cell;
	}

	/**
	 * Get fractional coordinates of this atom.
	 * @return Fractional coordinates of atom
	 */
	public double[] getPosition() {
		return Position;
	}

	/**
	 * Get Cartesian coordinates of this atom
	 * @return Cartesian coordinates of atom
	 */
	public double[] getPositionCartesian() {
		return PositionCartesian;
	}

	/**
	 * Set radius of atom
	 * @param Radius Radius
	 */
	public void setRadius(double Radius) {
		this.Radius = Radius;
	}

	/**
	 * Get radius of atom
	 * @return Radius
	 */
	public double getRadius() {
		return Radius;
	}

	/**
	 * Get the ID number of this atom
	 * @return ID number
	 */
	public int getID() {
		return ID;
	}

	/**
	 * Get type of this atom.
	 * @return Type of atom
	 */
	public int getType() {
		return Type;
	}

    /**
	 * Get type of this atom.
	 * @return Name of this atom type
	 */
	public String getTypeName() {
		return Cell.getTypeName(Type);
	}
}
