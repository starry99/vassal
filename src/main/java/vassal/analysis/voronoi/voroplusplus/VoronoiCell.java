package vassal.analysis.voronoi.voroplusplus;

import java.util.List;
import vassal.analysis.voronoi.BaseVoronoiCell;

/**
 * Implementation of Voronoi cell for Voro++
 * @author Logan Ward
 */
public class VoronoiCell extends BaseVoronoiCell {

    /**
     * Create a Voronoi cell, provided list of faces
     * @param atom Atom in center of cell
     * @param radical Whether this a radical plane tessellation
     * @param faces Faces on this cell
     */
    public VoronoiCell(vassal.data.Atom atom, boolean radical,
            List<VoronoiFace> faces) {
        super(atom, radical);

        // Add in the faces
        Faces.addAll(faces);
    }
}
