package vassal.analysis.toolkits;

import java.util.*;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import vassal.analysis.voronoi.BaseVoronoiCell;
import vassal.analysis.voronoi.VoronoiTessellationCalculator;
import vassal.data.Atom;
import vassal.data.Cell;

/**
 * Report statistics regarding the coordination polyhedra. Modeled after the interface
 * used by <a href="http://math.lbl.gov/voro++/">Voro++</a>.
 *
 * <p><b>Options</b>: &lt;atom|cell&gt; [-radical] [-radii &lt;name1&gt; &lt;radius1&gt; &lt;...&gt;]
 * <br><i>atom|cell</i>: Whether to print the coordination polyhedron of each atom,
 * or statistics regarding the entire cell.
 * <br><i>-radical</i>: Whether to use a radical-plane Voronoi tessellation
 * <br><i>nameN</i>: Symbol of element, use "*" to set a default value
 * <br><i>radiusN</i>: Radius of corresponding element. Should be in same units as the cell
 *
 * <p>Example Usage: atom -radical -radii Cu 1.26 Zr 1.54 * 1.4
 * <br>Will compute the radical Voronoi tessellation, with radii of 1.26 and 1.54 for Cu
 * and Zr, respecitively. Any other atoms with have a radius of 1.4. Statistics of
 * each atom will be printed.
 *
 * @author Logan Ward
 */
public class CoordinationPolyhedronToolkit extends BaseAnalysisToolkit {
    /** Report coordination of each atom or cell statistics */
    protected boolean PrintCellStatistics = false;
    /** Radii of each element*/
    final protected Map<String, Double> Radii = new TreeMap<>();
    /** Whether to compute a radical plane tessellation */
    protected boolean ComputeRadical = false;

    @Override
    public void setOptions(List<String> Options) throws Exception {
        // Clear current condition
        ComputeRadical = false;
        clearRadii();

        if (Options.isEmpty()) {
            throw new Exception(printUsage());
        }

        // Determine whether to print cell or atom statistics
        if (Options.get(0).equalsIgnoreCase("atom")) {
            setPrintCellStatistics(false);
        } else if (Options.get(0).equalsIgnoreCase("cell")) {
            setPrintCellStatistics(true);
        } else {
            throw new Exception(printUsage());
        }

        // If that is the only argument, return
        if (Options.size() == 1) {
            return;
        }

        // Check if next arg is radii
        int pos = 1;
        if (Options.get(pos).equals("-radical")) {
            setComputeRadicalTessellation(true);
            pos++;
        }

        // If there are any more arguments
        if (pos >= Options.size()) {
            return;
        }

        // Read in radii
        if (! Options.get(pos).equals("-radii")) {
            throw new Exception(printUsage());
        }
        pos++;

        try {
            while (pos < Options.size()) {
                // Parse value
                String elem = Options.get(pos++);
                double radius = Double.parseDouble(Options.get(pos++));

                // Add it to list
                addRadius(elem, radius);
            }
        } catch (Exception e) {
            throw new Exception(printUsage());
        }
    }

    @Override
    public String printUsage() {
        return "Usage: <atom|cell> [-radical] [-radii <element 1> <radis 1> <...>]";
    }

    /**
     * Set whether to print cell statistics or shape of cell around each atom.
     * @param x Desired setting
     */
    public void setPrintCellStatistics(boolean x) {
        this.PrintCellStatistics = x;
    }

    /**
     * Set whether to compute a radical plane Voronoi tessellation
     * @param x Desired setting
     */
    public void setComputeRadicalTessellation(boolean x) {
        this.ComputeRadical = x;
    }

    /**
     * Clear current list of radii
     */
    public void clearRadii() {
        Radii.clear();
    }

    /**
     * Set the radius for a certain element.
     * @param element Symbol of element. Use "*" to set a default value
     * @param radius Radius of element. Should be in same units as
     * {@linkplain Cell} object passed to {@linkplain #analyzeStructure(vassal.data.Cell) }
     */
    public void addRadius(String element, double radius) {
        Radii.put(element, radius);
    }

    @Override
    public String analyzeStructure(Cell cell) throws Exception {
        // Set radii, if any are set
        if (! Radii.isEmpty()) {
            for (Atom atom : cell.getAtoms()) {
                String type = atom.getTypeName();

                // Lookup radius
                if (Radii.containsKey(type)) {
                    atom.setRadius(Radii.get(type));
                } else if (Radii.containsKey("*")) {
                    atom.setRadius(Radii.get("*"));
                } else {
                    throw new Exception("No radius set for type: " + type);
                }
            }
        }

        // Compute Voronoi tessellation
        List<BaseVoronoiCell> cells = VoronoiTessellationCalculator
                .compute(cell, ComputeRadical);

        // Get coordination polyhedron shapes
        List<Map<Integer,Integer>> shapes = new ArrayList<>(cell.nAtoms());
        for (BaseVoronoiCell x : cells) {
            shapes.add(x.getPolyhedronShape());
        }
        int minEdges = Integer.MAX_VALUE;
        int maxEdges = Integer.MIN_VALUE;
        for (Map<Integer,Integer> shape : shapes) {
            for (Integer key : shape.keySet()) {
                minEdges = Math.min(key, minEdges);
                maxEdges = Math.min(key, maxEdges);
            }
        }
        minEdges = Math.min(minEdges, 3);
        maxEdges = Math.max(maxEdges, 6);

        // Decide what to print
        String output = "";
        if (PrintCellStatistics) {

            // Compute the frequency of each type of polyhedron
            Map<Pair<Integer,Map<Integer,Integer>>, Integer> tally;
            tally = new HashMap<>();
            for (int a=0; a<cell.nAtoms(); a++) {
                Pair<Integer,Map<Integer,Integer>> poly;
                poly = new ImmutablePair<>(cell.getAtom(a).getType(),
                    shapes.get(a));
                if (tally.containsKey(poly)) {
                    int newCount = tally.get(poly) + 1;
                    tally.put(poly, newCount);
                } else {
                    tally.put(poly, 1);
                }
            }

            // Sort it all out
            List<Map.Entry<Pair<Integer,Map<Integer,Integer>>,Integer>> polys;
            polys = new ArrayList<>(tally.entrySet());
            Collections.sort(polys,
                    new Comparator<Map.Entry<Pair<Integer, Map<Integer, Integer>>, Integer>>() {
                @Override
                public int compare(Map.Entry<Pair<Integer, Map<Integer, Integer>>, Integer> arg0,
                        Map.Entry<Pair<Integer, Map<Integer, Integer>>, Integer> arg1) {
                    return Integer.compare(arg0.getValue(), arg1.getValue());
                }
            });

            // Make the header
            output += "CenterType";
            for (int i=minEdges; i<=maxEdges; i++) {
                output += "    n" + i;
            }
            output += "  Count  Fraction\n";

            // Print it all out
            for (Map.Entry<Pair<Integer, Map<Integer, Integer>>, Integer> entrySet :
                    polys) {
                Pair<Integer, Map<Integer, Integer>> key = entrySet.getKey();
                Integer value = entrySet.getValue();
                output += String.format("%10s", cell.getTypeName(key.getKey()));
                for (int e=minEdges; e<=maxEdges; e++) {
                    int count = key.getRight().containsKey(e) ?
                            key.getRight().get(e) : 0;
                    output += String.format("  %4d", count);
                }
                output += String.format("  %5d  %-10.5e\n", value,
                        (double) value / cell.nAtoms());
            }
        } else {
            // Create header
            output += " AtomID  Type  Volume  SurfaceArea";
            for (int i=minEdges; i<=maxEdges; i++) {
                output += "    n" + i;
            }
            if (! Radii.isEmpty()) {
                output += "   PackingEfficiency";
            }
            output += "\n";

            // Print the cell statistics
            for (int i=0; i<cell.nAtoms(); i++) {
                String type = cell.getAtom(i).getTypeName();
                output += String.format("%7d  %4s  %6.3f  %11.3f",
                        i, type, cells.get(i).getVolume(),
                        cells.get(i).getSurfaceArea());
                for (int e=minEdges; e<=maxEdges; e++) {
                    int count = shapes.get(i).containsKey(e) ?
                            shapes.get(i).get(e) : 0;
                    output += String.format("  %4d", count);
                }
                if (! Radii.isEmpty()) {
                    output += String.format("   %.2f", 100 *
                            (4 / 3 * Math.PI * Math.pow(cell.getAtom(i).getRadius(), 3))
                             / cells.get(i).getVolume());
                }
                output += "\n";
            }
        }

        return output;
    }
}
