package vassal.data;

import java.util.*;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.*;

/**
 * Represents the volume enclosing a set of atoms. Could represent a crystal unit
 *  cell or the simulation cell from a molecular dynamics calculation.
 * @author Logan Ward
 */
public class Cell implements java.io.Serializable, java.lang.Cloneable {
    /**
     * Vectors representing sides of cell. Stored in the format:
     *  <p>a<sub>x</sub> b<sub>x</sub> c<sub>x</sub>
     * <br>a<sub>y</sub> b<sub>y</sub> c<sub>y</sub>
     * <br>a<sub>z</sub> b<sub>z</sub> c<sub>z</sub>
     */
	private RealMatrix SimulationCell;
    /**
     * Inverse of {@linkplain #SimulationCell}. Set during the
     * {@linkplain #setBasis(double[][]) }
     */
    private RealMatrix InverseCell;
	/** List of all atoms */
	private List<Atom> Atoms = new ArrayList<>();
	/** Whether the walls of the cell are periodic */
	private boolean[] FaceIsPeriodic;
	/** Names of each atom type */
	private List<String> TypeName = new ArrayList<>();
    /** Lattice vectors */
    private Vector3D[] LatticeVectors;
    /** Reciprocal lattice vectors */
    private Vector3D[] RecipLatticeVectors;

    /**
     * Create an empty structure a = b = c = 1, &alpha; = &beta; = &gamma; = 90, and
     * periodic boundary conditions in all directions.
     */
    public Cell() {
		double[][] basis = new double[3][3];
        for (int i=0; i < 3; i++) {
            basis[i][i] = 1.0;
        }
		try {
			SimulationCell = MatrixUtils.createRealIdentityMatrix(3);
			setBasis(basis);
		} catch (Exception e) {
			throw new Error("Pigs must be flying.");
		}
        FaceIsPeriodic = new boolean[3];
        Arrays.fill(FaceIsPeriodic, true);
    }

    @Override
    public Cell clone() {
        Cell x;
        try {
            x = (Cell) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error(e);
        }
        x.Atoms = new ArrayList<>(Atoms.size());
        for (Atom atom : Atoms) {
            Atom nAtom = atom.clone();
            nAtom.setCell(x);
            x.Atoms.add(nAtom);
        }
        x.FaceIsPeriodic = FaceIsPeriodic.clone();
        x.InverseCell = InverseCell.copy();
        x.SimulationCell = SimulationCell.copy();
        x.LatticeVectors = LatticeVectors.clone();
        x.RecipLatticeVectors = RecipLatticeVectors.clone();
        x.TypeName = new ArrayList<>(TypeName);
        return x;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Cell) {
            Cell other = (Cell) obj;
            // Check to see if number of atoms is the same
            if (nAtoms() != other.nAtoms()) return false;
            // Compare basis
            RealMatrix diff = SimulationCell.subtract(other.SimulationCell);
            if (diff.getNorm() > 1e-4) {
                return false;
            }
            // Compare atoms
            for (Atom atom1 : Atoms) {
                // See if *any* atom in other matches this one regardless of ID
                boolean found = false;
                for(Atom atom2 : other.Atoms) {
                    if (atom1.equals(atom2)) {
                        found = true;
                    }
                    if (found) break;
                }
                if (!found) return false;
            }
            return true;
        }
        return false;
    }



    /**
     * Define the basis. Format:
     *  <p>a<sub>x</sub> b<sub>x</sub> c<sub>x</sub>
     * <br>a<sub>y</sub> b<sub>y</sub> c<sub>y</sub>
     * <br>a<sub>z</sub> b<sub>z</sub> c<sub>z</sub>
     * @param basis New basis
     * @throws Exception If basis is incorrectly formatted
     */
    final public void setBasis(double[][] basis) throws Exception {
        // Check input
        if (basis.length != 3) {
            throw new Exception("Expected 3x3 matrix");
        }
        for (int i=0; i < 3; i++) {
            if (basis[0].length != 3)
                throw new Exception("Expected 3x3 matrix");
        }

        // Store old basis
        RealMatrix oldCell = SimulationCell.copy();
        SimulationCell = new BlockRealMatrix(basis);

        // Make sure it has positive volume
        if (volume() <= 0 || Double.isNaN(volume())) {
            SimulationCell = oldCell;
            throw new Exception("Provided basis has non-positive volume");
        }

		// Get the inverse
		InverseCell = new LUDecomposition(SimulationCell).getSolver().getInverse();

        // Update Cartesian coordinates of each atom
        for (Atom atom : Atoms) {
            atom.updateCartesianCoordinates();
        }

        // Precompute the lattice vectors
        LatticeVectors = new Vector3D[3];
		for (int i=0; i < 3; i++) {
			LatticeVectors[i] = new Vector3D(SimulationCell.getEntry(0, i),
					SimulationCell.getEntry(1, i),
					SimulationCell.getEntry(2, i));
		}
        RecipLatticeVectors = new Vector3D[3];
		for (int i=0; i < 3; i++) {
			RecipLatticeVectors[i] = new Vector3D(InverseCell.getEntry(0, i),
					InverseCell.getEntry(1, i),
					InverseCell.getEntry(2, i));
		}
    }

    /**
     * Define the basis vectors
     * @param lengths a, b, c
     * @param angles &alpha;, &beta;, &gamma; in degrees
     * @throws java.lang.Exception
     */
    public void setBasis(double[] lengths, double[] angles) throws Exception {
        // Check input
        if (lengths.length != 3) throw new Exception("Expected 3 lengths");
        if (angles.length != 3) throw new Exception("Expected 3 angles");

        // Convert angles to radians
        double[] anglesRadians = new double[3];
        for (int i=0; i < 3; i++) {
            if (angles[i] < 0 || angles[i] > 180) {
                throw new Exception("Angles must be between 0 and 180. Angle #"
                        + i + " = " + angles[i]);
            }
            anglesRadians[i] = Math.toRadians(angles[i]);
        }

        double[][] basis = computeBasis(lengths, anglesRadians);

        // Set the basis
        setBasis(basis);
    }

    /**
     * Compute the basis vectors given lengths and angles (in radians). The a
     * lattice vectors will be aligned in x direction, and the b will be in the
     * xy plane
     *
     * @param lengths Lengths of lattice vectors
     * @param anglesRadians Lattice angles in radians
     * @return One possible set of basis vectors. Format:
     *  <p>a<sub>x</sub> b<sub>x</sub> c<sub>x</sub>
     * <br>a<sub>y</sub> b<sub>y</sub> c<sub>y</sub>
     * <br>a<sub>z</sub> b<sub>z</sub> c<sub>z</sub>
     * @see #SimulationCell
     */
    static public double[][] computeBasis(double[] lengths, double[] anglesRadians) {
        // Convert lengths to basis
        double[][] basis = new double[3][3];
        basis[0][0] = lengths[0];
        basis[0][1] = lengths[1] * Math.cos(anglesRadians[2]);
        basis[0][2] = lengths[2] * Math.cos(anglesRadians[1]);
        basis[1][1] = lengths[1] * Math.sin(anglesRadians[2]);
        basis[1][2] = lengths[2] * (Math.cos(anglesRadians[0])
                - Math.cos(anglesRadians[1]) * Math.cos(anglesRadians[2]))
                / Math.sin(anglesRadians[2]);
        double v = Math.sqrt(1 - Math.cos(anglesRadians[0]) * Math.cos(anglesRadians[0])
                - Math.cos(anglesRadians[1]) * Math.cos(anglesRadians[1])
                - Math.cos(anglesRadians[2]) * Math.cos(anglesRadians[2])
                + 2 * Math.cos(anglesRadians[0]) * Math.cos(anglesRadians[1]) * Math.cos(anglesRadians[2]));
        basis[2][2] = lengths[2] * v / Math.sin(anglesRadians[2]);
        return basis;
    }

    /**
     * Add atom to cell
     *
     * @param atom Atom to be added
     */
    public void addAtom(Atom atom) {
        Atoms.add(atom);
		atom.setID(Atoms.size()-1);
		atom.setCell(this);
		while (TypeName.size() <= atom.getType()) {
			TypeName.add(null);
		}
    }

	/**
	 * Whether a certain direction has boundary conditions
	 * @param index Desired direction (0: x, 1: y, 2: z)
	 * @return Whether that direction has periodic boundary conditions
	 */
	public boolean directionIsPeriodic(int index) {
		return FaceIsPeriodic[index];
	}

    /**
     * Volume of the simulation cell
     * @return Volume of cell
     */
    public double volume() {
        double[][] basis = SimulationCell.getData();
        return basis[0][0] * (basis[1][1] * basis[2][2] - basis[2][1] * basis[1][2])
                - basis[0][1] * (basis[1][0] * basis[2][2] - basis[1][2] * basis[2][0])
                + basis[0][2] * (basis[1][0] * basis[2][1] - basis[1][1] * basis[2][0]);
    }

    /**
     * Get basis of this structure
     * @return Basis of simulation cell
     * @see #SimulationCell
     */
    public double[][] getBasis() {
        return SimulationCell.getData();
    }

    /**
     * Get basis of this structure
     * @return Basis of simulation cell
     * @see #SimulationCell
     */
    public RealMatrix getBasisMatrix() {
        return SimulationCell.copy();
    }

    /**
     * Get the inverse basis.
     * @return Inverse of {@linkplain #SimulationCell}.
     */
    public double[][] getInverseBasis() {
        return InverseCell.getData();
    }

	/**
	 * Get the lattice vectors for this cell
	 * @return Array of lattice vectors (0: a, 1: b, 2: c)
	 */
	public Vector3D[] getLatticeVectors() {
		return LatticeVectors;
	}

    /**
     * Get the lengths of each lattice vector
     * @return Lattice parameters
     */
    public double[] getLatticeParameters() {
        double[] output = new double[3];
        for (int d=0; d<3; d++) {
            output[d] = SimulationCell.getColumnVector(d).getNorm();
        }
        return output;
    }

    /**
     * Get angles between lattice vectors
     * @return Lattice angles in radians
     */
    public double[] getLatticeAnglesRadians() {
        double[] output = new double[3];

        // Compute cosines
        output[0] = SimulationCell.getColumnVector(1).cosine(SimulationCell.getColumnVector(2));
        output[1] = SimulationCell.getColumnVector(2).cosine(SimulationCell.getColumnVector(0));
        output[2] = SimulationCell.getColumnVector(0).cosine(SimulationCell.getColumnVector(1));

        // Convert to radians
        for (int d=0; d<3; d++) {
            output[d] = Math.acos(output[d]);
        }

        return output;
    }

    /**
     * Get angles between lattice vectors
     * @return Lattice angles in degrees
     */
    public double[] getLatticeAngles() {
        double[] output = getLatticeAnglesRadians();
        for (int d=0; d<3; d++) {
            output[d] = Math.toDegrees(output[d]);
        }
        return output;
    }

    /**
     * Get basis vectors aligned such that the a vector is parallel to the x axis,
     * and the b vector is in the x-y plane. Format:
     *  <p>a<sub>x</sub> b<sub>x</sub> c<sub>x</sub>
     * <br>a<sub>y</sub> b<sub>y</sub> c<sub>y</sub>
     * <br>a<sub>z</sub> b<sub>z</sub> c<sub>z</sub>
     * @return Aligned basis vectors.
     */
    public double[][] getAlignedBasis() {
        double[] angles = getLatticeAnglesRadians();
        double[] lengths = getLatticeParameters();
        return computeBasis(lengths, angles);
    }

    /**
	 * Get the reciprocal lattice vectors for this cell. These are simply the matrix inverse
     * of the lattice vectors.
	 * @return Array of reciprocal lattice vectors (0: a, 1: b, 2: c)
	 */
	public Vector3D[] getReciprocalLatticeVectors() {
		return RecipLatticeVectors;
	}

	/**
	 * Get all atoms in the structure
	 * @return List of atoms
	 */
	public List<Atom> getAtoms() {
		return new LinkedList<>(Atoms);
	}

    /**
     * Get a single atom
     * @param index Index of atom
     * @return Link to that atom
     */
    public Atom getAtom(int index) {
        return Atoms.get(index);
    }

    /**
     * Number of atoms in simulation cell
     *
     * @return Number of atoms
     */
    public int nAtoms() {
        return Atoms.size();
    }

	/**
	 * Number of atom types
	 * @return Number of types
	 */
	public int nTypes() {
		return TypeName.size();
	}

	/**
	 * Get the number of atoms of a certain type
	 * @param type Type index
	 * @return Number of atoms
	 */
	public int numberOfType(int type) {
		int output = 0;
		for (Atom atom : Atoms) {
			if (atom.getType() == type) {
				output++;
			}
		}
		return output;
	}

    /**
     * Add a new atom type to this cell.
     * @param name Name of type (can be null)
     */
    public void addType(String name) {
        TypeName.add(name);
    }

	/**
	 * Define the name for an atom type. Note that indexing starts at 0
	 * @param index Index of atom type
	 * @param name Desired name
	 */
	public void setTypeName(int index, String name) {
		TypeName.set(index, name);
	}

    /**
     * Change the name of several types at once.
     * @param changes Changes to be made. Key: Current Name, Value: New Name
     */
    public void replaceTypeNames(Map<String,String> changes) {
        // Get the type IDs corresponding to each name
        Map<String,List<Integer>> nameMap = new TreeMap<>();
        for (String name : changes.keySet()) {
            nameMap.put(name, new ArrayList<Integer>());
        }
        for (int id=0; id<TypeName.size(); id++) {
            String name = TypeName.get(id);
            for (Map.Entry<String, List<Integer>> byName : nameMap.entrySet()) {
                if (byName.getKey().equals(name)) {
                    byName.getValue().add(id);
                }
            }
        }

        // Make the replacements
        for (Map.Entry<String, List<Integer>> byName : nameMap.entrySet()) {
            String newName = changes.get(byName.getKey());
            if (newName == null) {
                continue;
            }
            for (Integer id : byName.getValue()) {
                TypeName.set(id, newName);
            }
        }
    }

    /**
     * For types that have the same name, and combine them.
     */
    public void mergeLikeTypes() {
        int curType = 0;
        while (curType < TypeName.size()) {
            // Skip if name is null
            String curName = getTypeName(curType);
            if (curName == null) {
                curType++;
                continue;
            }

            // Look to see if there is a duplicate name
            int matchedType = -1;
            for (int t=0; t < nTypes(); t++) {
                if (t == curType) continue;
                String name = getTypeName(t);
                if (name == null) continue;
                if (name.equals(curName)) {
                    matchedType = t;
                    break;
                }
            }

            // If not matched, increment and move on
            if (matchedType == -1) {
                curType++;
                continue;
            }

            // Merge (matchTyped -> curType) (a > matchedType -> a-1)
            TypeName.remove(matchedType);
            for (Atom atom : Atoms) {
                int type = atom.getType();
                if (type == matchedType) {
                    atom.setType(curType);
                } else if (type > matchedType) {
                    atom.setType(type - 1);
                }
            }
        }
    }

	/**
	 * Set the radius of all atoms of a certain type
	 * @param index Certain type
	 * @param radius Desired radius
	 */
	public void setTypeRadius(int index, double radius) {
		for (Atom atom : Atoms) {
			if (atom.getType() == index) {
				atom.setRadius(radius);
			}
		}
	}

	/**
	 * Get name of atom type
	 * @param index Index of atom type
	 * @return Name of atom, index if type is not yet named
	 */
	public String getTypeName(int index) {
        if (index >= TypeName.size()) {
            return Integer.toString(index);
        }
		String name = TypeName.get(index);
		return name == null ? Integer.toString(index) : name;
	}

	/**
	 * Convert fractional coordinates to Cartesian
	 * @param x Fractional coordinates
	 * @return Cartesian coordinates
	 */
	public double[] convertFractionalToCartestian(double[] x) {
		double[] output = new double[3];
		for (int i=0; i < 3; i++) {
			output[i] = x[0] * SimulationCell.getEntry(i, 0)
					+ x[1] * SimulationCell.getEntry(i, 1)
					+ x[2] * SimulationCell.getEntry(i, 2);
		}
		return output;
	}

	/**
	 * Convert Cartesian coordinates to fractional
	 * @param x Cartesian coordinates
	 * @return Fractional coordinates
	 */
	public double [] convertCartesianToFractional(double[] x) {
		RealVector cart = new ArrayRealVector(x);
		RealVector frac = InverseCell.operate(cart);
		return frac.toArray();
	}

	/**
	 * Given position in Cartesian coordinates, compute position periodic image.
	 * @param position Position in Cartesian coordinates
	 * @param x Number of steps in X direction
	 * @param y Number of steps in Y direction
	 * @param z Number of steps in Z direction
	 * @return New position
	 */
	public double[] getPeriodicImage(double[] position, int x, int y, int z) {
		double[] output = position.clone();
		output[0] += x * SimulationCell.getEntry(0, 0)
				+ y * SimulationCell.getEntry(0, 1)
				+ z * SimulationCell.getEntry(0, 2);
		output[1] += x * SimulationCell.getEntry(1, 0)
				+ y * SimulationCell.getEntry(1, 1)
				+ z * SimulationCell.getEntry(1, 2);
		output[2] += x * SimulationCell.getEntry(2, 0)
				+ y * SimulationCell.getEntry(2, 1)
				+ z * SimulationCell.getEntry(2, 2);
		return output;
	}

	/**
	 * Get the minimum distance between any images of two points.
     *
     * <p>Algorithm:
     * <ol>
     * <li>Compute the displacement between the points in Cartesian units
     * <li>For each lattice vector:
     * <ol>
     * <li>Compute the project of the displacement onto this vector
     * <li>Divide that distance by the length of the lattice vector, and round to
     * get the number of extra lattice vectors away this displacement vector is
     * from the zero vector.
     * <li>Multiply the lattice vector by that factor and subtract the result
     * from the current lattice vector to get a candidate displacement vector.
     * <li>If this vector is shorter than the current displacement vector,
     * replace this vector with the new one.
     * </ol>
     * <li>Compute the distance of this displacement vector
     * </ol>
	 * @param point1 Fractional coordinates of point #1
	 * @param point2 Fractional coordinates of point #2
	 * @return Minimum distance in Cartesian units
	 */
	public double getMinimumDistance(double[] point1, double[] point2) {
        // Get distance in cartesian units
        double[] disp = convertFractionalToCartestian(point2);
        double[] point1Cart = convertCartesianToFractional(point1);
        for (int i=0; i<3; i++) {
            disp[i] -= point1Cart[i];
        }

        // Get lattice Vectors
        Vector3D[] latVec = getLatticeVectors();

        // For each direction, find the closest image
        double curDist = (disp[0] * disp[0] + disp[1] * disp[1]
                + disp[2] * disp[2]);
        for (int d=0; d<3; d++) {
            double projD = latVec[d].getX() * disp[0] +
                    latVec[d].getY() * disp[1] +
                    latVec[d].getZ() * disp[2];
            projD /= latVec[d].getNormSq();
            long nSteps = Math.round(projD);
            double[] newDisp = disp.clone();
            newDisp[0] -= nSteps * latVec[d].getX();
            newDisp[1] -= nSteps * latVec[d].getY();
            newDisp[2] -= nSteps * latVec[d].getZ();
            double newDist = (newDisp[0] * newDisp[0]
                    + newDisp[1] * newDisp[1]
                    + newDisp[2] * newDisp[2]);
            if (newDist < curDist) {
                disp = newDisp;
                curDist = newDist;
            }
        }

		return Math.sqrt(curDist);
	}

    /**
     * Get the closest image of a neighboring atom to an atom.
     * @param center ID of central atom
     * @param neighbor Neighboring atom
     * @return Atom image of neighbor atom that is closest to the central atom
     */
    public AtomImage getClosestImage(int center, int neighbor) {
        Atom centerAtom = getAtom(center);
        Atom neighboringAtom = getAtom(neighbor);

        // Compute the displacement vector between these atoms
        double[] disp = neighboringAtom.getPositionCartesian().clone();
        double[] centerPos = centerAtom.getPositionCartesian();
        for (int i=0; i<3; i++) {
            disp[i] -= centerPos[i];
        }

        // Find the closest image
        int[] image = new int[3];
        Vector3D[] latVec = getLatticeVectors();
        double curDist = disp[0] * disp[0] + disp[1] * disp[1] + disp[2] * disp[2];
        for (int d=0; d<3; d++) {
            double projD  = (latVec[d].getX() * disp[0]
                    + latVec[d].getY() * disp[1]
                    + latVec[d].getZ() * disp[2]) / latVec[d].getNormSq();
            long nSteps = Math.round(projD);
            double[] newDisp = disp.clone();
            newDisp[0] -= nSteps * latVec[d].getX();
            newDisp[1] -= nSteps * latVec[d].getY();
            newDisp[2] -= nSteps * latVec[d].getZ();
            double newDist = newDisp[0] * newDisp[0]
                    + newDisp[1] * newDisp[1] + newDisp[2] * newDisp[2];
            if (newDist < curDist) {
                disp = newDisp;
                image[d] = -1 * (int) nSteps;
                curDist = newDist;
            }
        }

        return new AtomImage(neighboringAtom, image);
    }
}
