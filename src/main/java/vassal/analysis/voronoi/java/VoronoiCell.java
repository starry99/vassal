package vassal.analysis.voronoi.java;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import org.apache.commons.lang3.tuple.Pair;
import vassal.data.Atom;
import org.apache.commons.math3.geometry.euclidean.threed.*;
import vassal.analysis.PairDistanceAnalysis;
import vassal.analysis.voronoi.BaseVoronoiCell;
import vassal.analysis.voronoi.BaseVoronoiEdge;
import vassal.analysis.voronoi.BaseVoronoiFace;
import vassal.analysis.voronoi.BaseVoronoiVertex;
import vassal.analysis.voronoi.output.EdgeWriter;
import vassal.data.AtomImage;

/**
 * Representation of a Voronoi cell around an atom.
 *
 * <p><b>Computing a Voronoi Cell</b>
 * <ol>
 * <li>Initialize the cell by calling {@linkplain #VoronoiCell(jstrc.data.Atom) },
 * which will create a Voronoi cell with faces corresponding to the [+/-][x/y/z]
 * images of this atom for periodic boundary conditions or the walls of the simulation
 * cell for non-periodic calculations.
 * <li>Add new faces to the cell using either {@linkplain #addFacesFromAtom(jstrc.data.Atom, jstrc.analysis.voronoi.VoronoiCell) }
 * to add all faces from a certain atom (and its periodic images), or
 * {@linkplain #addNewFace(jstrc.analysis.voronoi.VoronoiFace) } for a face
 * from anonymous sources.
 * </ol>
 * @author Logan Ward
 */
public class VoronoiCell extends BaseVoronoiCell {
    /** Whether to run in debug mode */
    final private boolean Debug = false;

    public VoronoiCell(Atom atom, boolean radical) {
        super(atom, radical);
    }

    /**
     * Compute cell, given ability to generate images
     * @param imageFinder Tool to find images within a cutoff
     * @param cutoff Initial cutoff. Will be increased if too small
     * @throws java.lang.Exception
     */
    public void computeCell(PairDistanceAnalysis imageFinder, double cutoff)
            throws Exception {
        double curCutoff = cutoff;
        int nAttempts = 0;
        while (nAttempts++ < 4) {
            imageFinder.setCutoffDistance(curCutoff);
            // Find all nearby images
            List<AtomImage> images = new LinkedList<>();
            for (Pair<AtomImage,Double> image :
                    imageFinder.getAllNeighborsOfAtom(Atom.getID())) {
                images.add(image.getKey());
            }

            // Compute cell
            try {
                computeCell(new ArrayList<>(images));
            } catch (Exception e) {
                curCutoff *= 1.5;
                continue;
            }
            return;
        }

        throw new Exception("Cell failed to compute");
    }

    /**
     * Compute the Voronoi cell, given list of images
     * @param images [in] List of images to consider, where key is the atomID
     * and value is the distance
     * @throws java.lang.Exception
     */
    public void computeCell(List<AtomImage> images) throws Exception {
        // Clear cached volume
        Volume = Double.NaN;

        // Get all possible faces
        List<VoronoiFace> possibleFaces = computeFaces(images);

        // Get the faces corresponding to the direct polyhedron
        List<VoronoiFace> directFaces = computeDirectNeighbors(possibleFaces);

        // Construct direct polyhedron
        for (VoronoiFace directFace : directFaces) {
            try {
                directFace.assembleFaceFromFaces(directFaces);
            } catch (Exception e) {
                throw new Exception("Direct polyhedron failed to construct.");
            }
        }
        Faces.addAll(directFaces);

        if (Debug) {
            try {
                new EdgeWriter().writeToFile(this, "debug/direct.edges");
            } catch (IOException e) {
                // Print output
            }
        }

        // Get the faces that might actually be direct faces
        List<VoronoiFace> possibleIndirectFaces
                = computePossibleIndirectNeighbors(possibleFaces);

        // Use these faces to compute indirect neighbors
        for (VoronoiFace face : possibleIndirectFaces) {
            computeIntersection(face);
        }
    }

    /**
     * Compute the center of the face corresponding to each neighbor
     * @param images List of all images of this atom
     * @return List of faces
     */
    protected List<VoronoiFace> computeFaces(List<AtomImage> images) {
        // Generate faces
        List<VoronoiFace> output = new ArrayList(images.size());
        for (AtomImage image : images) {
            // Check if the image is this atom
            if (image.getAtomID()== Atom.getID() &&
                    Arrays.equals(image.getSupercell(), new int[]{0,0,0})) {
                continue; // If so, skip
            }

            // Make the appropriate face
            try {
                output.add(new VoronoiFace(Atom, image, Radical));
            } catch (Exception e) {
                throw new Error("Failure to create face.");
            }
        }
        return output;
    }

    /**
     * Compute list of neighbors for the direct polyhedron and those that could
     * contribute to the Voronoi polyhedron.
     *
     * <p>See page 85 of <a href="http://linkinghub.elsevier.com/retrieve/pii/0021999178901109">
     * Brostow, Dessault, Fox. JCP (1978)</a>
     *
     * @param faces [in/out] List of faces to consider. After this operation,
     * any faces that are on the direct polyhedron will be removed and the list
     * will be sorted by distance from the center.
     * @return List of faces that are on the direct polyhedron
     */
    protected List<VoronoiFace> computeDirectNeighbors(List<VoronoiFace> faces) {
        // Sort by distance from face to the center
        Collections.sort(faces, new Comparator<VoronoiFace>() {
            @Override
            public int compare(VoronoiFace o1, VoronoiFace o2) {
                return Double.compare(o1.getFaceDistance(), o2.getFaceDistance());
            }
        });

        // The closest face is on the direct polyhedron
        List<VoronoiFace> directFaces = new LinkedList<>();
        directFaces.add(faces.remove(0));

        // Now, loop through all faces to find those whose centers are not
        //  outside any other face that is on the direct polyhedron
        Iterator<VoronoiFace> iter = faces.iterator();
        while (iter.hasNext()) {
            VoronoiFace face = iter.next();
            boolean isInside = true;
            for (VoronoiFace directFace : directFaces) {
                if (directFace.positionRelativeToFace(face.getFaceCenter()) >= 0) {
                    isInside = false;
                }
            }
            if (isInside) {
                iter.remove();
                directFaces.add(face);
            }
        }
        return directFaces;
    }

    /**
     * Get list of faces that might possibly be indirect. You must already have
     * computed direct faces and all vertices of the direct polyhedron.
     * @param possibleFaces [in] List of all possible non-direct faces sorted
     * in ascending order. This probably has already been computed by
     * {@linkplain #computeDirectNeighbors(java.util.List) }
     * @return List of faces that might actually be indirect neighbors. Here,
     * we find if the face distance is less than the farthest vertex on the
     * direct polyhedron.
     */
    protected List<VoronoiFace> computePossibleIndirectNeighbors(List<VoronoiFace> possibleFaces) {
        // Get list of faces that might be indirect neighbors
        double maxVertexDistance = 0;
        for (BaseVoronoiFace face : Faces) {
            for (BaseVoronoiVertex vertex : face.getVertices()) {
                maxVertexDistance = Math.max(maxVertexDistance, vertex.getDistanceFromCenter());
            }
        }
        List<VoronoiFace> possibleIndirectFaces = new LinkedList<>();
        for (VoronoiFace face : possibleFaces) {
            if (face.getFaceDistance() < maxVertexDistance) {
                possibleIndirectFaces.add(face);
            } else {
                break; // possibleFaces is sorted by getDirectFaces
            }
        }
        return possibleIndirectFaces;
    }

    /**
     * Compute intersection between a potential face and this cell
     * @param newFace Potential new face
     * @return Whether it intersected this cell
     */
    public boolean computeIntersection(VoronoiFace newFace) {
        // Clear cached result
        Volume = Double.NaN;

        // Debug operation: Print cell before cut
        if (Debug) {
            try {
                new EdgeWriter().writeToFile(this, "debug/before-cut.edges");
                PrintWriter fp = new PrintWriter(
                        new FileOutputStream("debug/before-cut.edges", true));
                fp.print("center: ");
                fp.format("%.6e %.6e %.6e\n", newFace.getFaceCenter().getX(),
                        newFace.getFaceCenter().getY(), newFace.getFaceCenter().getZ());
                Plane plane = newFace.getPlane();
                fp.print("u: ");
                fp.format("%.6e %.6e %.6e\n", plane.getU().getX(),
                        plane.getU().getY(), plane.getU().getZ());
                fp.print("v: ");
                fp.format("%.6e %.6e %.6e\n", plane.getV().getX(),
                        plane.getV().getY(), plane.getV().getZ());
                fp.close();
            } catch (Exception e) {
                // Do nothing: No debugging
            }
        }

        // Determine whether any faces intersect this new face
        boolean faceDoesntIntersect = true;
        for (BaseVoronoiFace currentFace : Faces) {
            for (BaseVoronoiVertex v : currentFace.getVertices()) {
                if (newFace.positionRelativeToFace(v.getPosition()) > 0) {
                    faceDoesntIntersect = false;
                    break;
                }
            }
        }
        if (faceDoesntIntersect) {
            return false;
        }

        // If it does, store the old face information
        Map<BaseVoronoiFace, List<BaseVoronoiEdge>> previousState = new TreeMap<>();
        for (BaseVoronoiFace face : Faces) {
            previousState.put(face, face.getEdges());
        }

        // Attempt to perform intersections
        try {
            List<BaseVoronoiEdge> newEdges = new ArrayList<>();
            for (BaseVoronoiFace cutFace : Faces) {
                VoronoiEdge newEdge = ((VoronoiFace) cutFace).computeIntersection(newFace);
                if (cutFace.NEdges() < 3) {
                    Faces.remove(cutFace);
                } else {
                    if (newEdge != null) {
                        newEdges.add(newEdge);
                    }
                }
            }

            // Check if we have enough edges
            if (newEdges.size() < 3) {
                throw new Exception("No enough edges were formed");
            }

            // Assemble new face and add it to list of faces
            newFace.assembleFaceFromEdges(newEdges);
            Faces.add(newFace);

            // Check of geometry error
            if (! geometryIsValid()) {
                throw new Exception();
            }

            // Debug operation: Print cell after cut
            if (Debug) {
                try {
                    new EdgeWriter().writeToFile(this, "debug/after-cut.edges");
                    PrintWriter fp = new PrintWriter(
                            new FileOutputStream("debug/after-cut.edges", true));
                    fp.print("center: ");
                    fp.format("%.6e %.6e %.6e\n", newFace.getFaceCenter().getX(),
                            newFace.getFaceCenter().getY(), newFace.getFaceCenter().getZ());
                    Plane plane = newFace.getPlane();
                    fp.print("u: ");
                    fp.format("%.6e %.6e %.6e\n", plane.getU().getX(),
                            plane.getU().getY(), plane.getU().getZ());
                    fp.print("v: ");
                    fp.format("%.6e %.6e %.6e\n", plane.getV().getX(),
                            plane.getV().getY(), plane.getV().getZ());
                    fp.close();
                } catch (Exception e) {
                    // Do nothing: No debugging
                }
            }

            if (Debug) {
                System.out.println(newFace.getArea());
            }
            return true;
        } catch (Exception e) {
            // Restore previous state
            Faces.clear();
            for (Map.Entry<BaseVoronoiFace, List<BaseVoronoiEdge>> entry : previousState.entrySet()) {
                BaseVoronoiFace face = entry.getKey();
                List<BaseVoronoiEdge> edges = entry.getValue();
                ((VoronoiFace) face).resetEdges(edges);
                Faces.add(face);
            }
            return false;
        }
    }

    /**
     * Remove a face from this cell
     * @param toRemove Face to remove
     * @throws java.lang.Exception
     */
    public void removeFace(VoronoiFace toRemove) throws Exception {
        if (! Faces.contains(toRemove)) {
            throw new Error("No such face.");
        }
        Faces.remove(toRemove);
        Volume = Double.NaN;

        // Find all faces that are currently in contact with this face
        Set<BaseVoronoiFace> contactingFaces = toRemove.getNeighboringFaces();
        for (BaseVoronoiFace faceToBuild : contactingFaces) {
            List<BaseVoronoiEdge> currentEdges = faceToBuild.getEdges();

            // Compute edges corresponding to the "contacting faces"
            for (BaseVoronoiFace face : contactingFaces) {
                if (face.equals(faceToBuild)) {
                    continue;
                }
                try {
                    VoronoiEdge newEdge = new VoronoiEdge((VoronoiFace) faceToBuild,
                            (VoronoiFace) face);
                    currentEdges.add(newEdge);
                } catch (Exception e) {
                    // Do nothing
                }
            }

            // Remove the edge corresponding to the face being removed
            Iterator<BaseVoronoiEdge> iter = currentEdges.iterator();
            while (iter.hasNext()) {
                if (iter.next().getIntersectingFace().equals(toRemove)) {
                    iter.remove();
                }
            }

            ((VoronoiFace) faceToBuild).assembleFaceFromEdges(currentEdges);
        }
    }

}
