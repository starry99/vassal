#! /bin/bash

# Compile Linux version
awk '{if (/CXX/) { print "CXX=g++" } else { print $0 }}' config.mk > temp
mv temp config.mk
make clean all
g++ voroExt.cpp -I src/ src/libvoro++.a -o voroExt.exe

# Compile Windows version
awk '{if (/CXX/) { print "CXX=x86_64-w64-mingw32-g++" } else { print $0 }}' config.mk > temp
mv temp config.mk
make clean all
x86_64-w64-mingw32-g++ -static voroExt.cpp -I src/ src/libvoro++.a -o voroExt_windows.exe

# Compile MAC version
awk '{if (/CXX/) { print "CXX=g++" } else { print $0 }}' config.mk > temp
mv temp config.mk
make clean all
g++ voroExt.cpp -I voro++-0.4.6/src/ voro++-0.4.6/src/libvoro++.a -o voroExt_MACOS.exe
