// Tool designed to use Voro++ to compute a Voronoi tessellation
//  by passing in structural information via stdin. Expected format
//  
//  basis a_x, b_x, b_y, c_x, c_y, c_z 
//  periodic <true|false> <true|false> <true|false>
//  natoms <# of atoms>
//  poly <true|false>
//  atom1_x atom1_y atom1_z [atom1_r]
//  [...]
//
//  Notes:
//    - Atom positions are expected to be in fractional coordinates
// 
//  Prints the information regarding each cell to the screen as:
//
//  atom <atom #>
//  position <x> <y> <z>
//  volume <volume>
//  nvertices <# vertices>
//  v1_x v1_y v1_z
//  [...]
//  nfaces <# faces>
//  face1_neighbor face1_area face1_nx face1_ny face1_nz face1_vertex1 face1_vertex2 [...]
//  [...]
// 
//  Note: May not be in the same order as the input!
//
//  Author: Logan Ward (ward.logan.t@gmail.com)
//  Date: 21 July 2015
//

#include <iostream>
#include <cstdio>
#include <cstring>
#include <vector>
#include <container_prd.hh>
#include <c_loops.hh>
#include <cell.hh>

#define DEBUG 2 // Leave at 2, Vassal uses this output information
#define PRINT_PREC 10

/**
 * Print out expected format to stdout
 */
void printFormat() {
    puts("Expected format:\n"
        "\tbasis a_x, b_x, b_y, c_x, c_y, c_z\n"
        "\tperiodic <true|false> <true|false> <true|false>\n"
        "\tnatoms <# of atoms>\n"
        "\tpoly <true|false>\n"
        "\tatom1_x atom1_y atom1_z [atom1_r]\n"
        "\t[...]\n");
}

int main(int argc, char** argv) {
    // Define settings
    char temp[256];
    
    double a_x, b_x, b_y, c_x, c_y, c_z;
    bool periodic[3], poly;
    std::vector<double> atomX, atomY, atomZ, atomR;
    
    // Read in the basis vectors
    std::cin.getline(temp, 256);
    int nRead = sscanf(temp, "%*s %lf %lf %lf %lf %lf %lf", &a_x,
        &b_x, &b_y,
        &c_x, &c_y, &c_z);
    if (nRead < 6) {
        puts("Failure to read basis");
        printFormat();
        return 1;
    }
    if (DEBUG > 0) {
        puts("Basis:");
        printf("\ta: %.10f %.10f %.10f\n",a_x, 0.0, 0.0);
        printf("\tb: %.10f %.10f %.10f\n",b_x, b_y, 0.0);
        printf("\tc: %.10f %.10f %.10f\n",c_x, c_y, c_z);
    }
    
    // Read in periodicity
    std::cin >> temp; // periodic
    std::cout << "periodic:";
    for (int i=0; i<3; i++) {
        std::cin >> temp;
        if (strcmp(temp, "True") == 0 || strcmp(temp, "true") == 0) {
            periodic[i] = true;
            std::cout << " True";
        } else if (strcmp(temp, "False") == 0 || strcmp(temp, "false") == 0) {
            periodic[i] = false;
            std::cout << " False";
        } else {
            std::cerr << "Periodicity choice not recognized: " << temp << std::endl;
            return 4;
        }
    }
    std::cout << std::endl;
    std::cin.getline(temp, 256); // Read the rest of the line
    
    // Read in number of atoms
    std::cin.getline(temp, 256);
    int nAtoms;
    nRead = sscanf(temp, "%*s %d", &nAtoms);
    if (nRead != 1) {
        std::cout << "Failed to read number of atoms" << std::endl;
        printFormat();
        return 1;
    }
    atomX.reserve(nAtoms);
    atomY.reserve(nAtoms);
    atomZ.reserve(nAtoms);
    atomR.reserve(nAtoms);
    
    // Read in whether the data is polydisperse
    std::cin >> temp; // polydisperse
    std::cin >> temp; // True|False
    if (strcmp(temp, "True") == 0 || strcmp(temp, "true") == 0) {
        poly = true;
    } else if (strcmp(temp, "False") == 0 || strcmp(temp, "false") == 0) {
        poly = false;
    } else {
        std::cerr << "Polydispersity choice not recognized: " << temp << std::endl;
        return 4;
    }
    std::cout << "Polydisperse: ";
    if (poly) {
         std::cout << "True";
    } else {
         std::cout << "False";
    }
    std::cout << std::endl;
    std::cin.getline(temp, 256); // Read the rest of the line
        
    // Read in the atom data
    printf("Number of atoms: %d\n", nAtoms);
    for (int a=0; a<nAtoms; a++) {
        double x,y,z,r;
        std::cin.getline(temp, 256); // Read the rest of the line
        
        // Parse the structure
        if (poly) {
            nRead = sscanf(temp, "%lf %lf %lf %lf", &x, &y, &z, &r);
            if (nRead != 4) {
                std::cout << "Error when reading atom #" << a << std::endl;
                return 2;
            }
        } else {
            nRead = sscanf(temp, "%lf %lf %lf", &x, &y, &z);
            r = 1;
            if (nRead != 3) {
                std::cout << "Error when reading atom #" << a << std::endl;
                return 2;
            }
        }
        
        // Store results
        atomX.push_back(x * a_x + y * b_x + z * c_x);
        atomY.push_back(y * b_y + z * c_y);
        atomZ.push_back(z * c_z);
        atomR.push_back(r);
    }
    
    // Create the cell
    voro::container_periodic_poly box(a_x, b_x, b_y, c_x, c_y, c_z, 
        1, 1, 1, nAtoms);
    for (int a=0; a<nAtoms; a++) {
        box.put(a, atomX[a], atomY[a], atomZ[a], atomR[a]);
    }
    
    // Output cell information
    voro::c_loop_all_periodic loop(box);
    loop.start();
    for (int a=0; a<nAtoms; a++) {
        
        // Get current position
        int atom; 
        double x, y, z, r;
        loop.pos(atom, x, y, z, r);
        printf("atom %d\nposition %f %f %f\n", atom, x, y, z);
        std::cout.flush();
        
        // Compute the Voronoi cell
        voro::voronoicell_neighbor cell;
        if (!box.compute_cell(cell, loop)) {
            printf("Tessellation failure!\n");
            return 3;
        }
        
        // Print the volume
        printf("volume %.10f\n", cell.volume());
        
        // Print the vertices
        std::vector<double> verts;
        cell.vertices(x, y, z, verts);
        printf("nverts %lu\n", verts.size() / 3);
        for (int v=0; v<verts.size() / 3; v++) {
            printf("%.10f %.10f %.10f\n", verts[v*3 + 0],
                    verts[v*3 + 1], verts[v*3 + 2]);
        }
        
        // Print out number of faces
        printf("nfaces %d\n", cell.number_of_faces());
        
        // Gather face information
        std::vector<int> neigh, faceVerts;
        cell.neighbors(neigh);
        cell.face_vertices(faceVerts);
        std::vector<double> area, normal;
        cell.face_areas(area);
        cell.normals(normal);
        
        // Print out face information
        int fvPos = 0;
        for (int f=0; f<cell.number_of_faces(); f++) {
        	printf("%d %.10lf", neigh[f], area[f]); // Neigh, area
        	for (int d=0; d<3; d++) {
        		printf(" %.10lf", normal[3*f + d]); // Normal
        	}
        	int f_order = faceVerts[fvPos++];
        	for (int fv=0; fv<f_order; fv++) {
        		printf(" %d", faceVerts[fvPos++]);
        	}
        	printf("\n");
        }
        
        // Increment loop counter
        loop.inc();
    }
}
