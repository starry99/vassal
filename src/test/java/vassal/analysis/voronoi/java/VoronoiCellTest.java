package vassal.analysis.voronoi.java;

import java.util.LinkedList;
import java.util.List;
import org.apache.commons.math3.geometry.euclidean.threed.Plane;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.Test;
import vassal.analysis.voronoi.VoronoiTessellationCalculator;
import static org.junit.Assert.*;
import vassal.analysis.voronoi.BaseVoronoiCell;
import vassal.analysis.voronoi.BaseVoronoiFace;
import vassal.analysis.voronoi.output.EdgeWriter;
import vassal.data.Atom;
import vassal.data.Cell;
import vassal.data.AtomImage;
import vassal.util.VectorCombinationComputer;

/**
 *
 * @author Logan Ward
 */
public class VoronoiCellTest {

    @Test
    public void scTest() throws Exception {
        // Create the cell
        Cell strc = new Cell();
        strc.addAtom(new Atom(new double[]{0,0,0}, 0));

        // Create cell for atom 1
        VoronoiCell cell = new VoronoiCell(strc.getAtom(0), true);

        // Compute faces
        List<AtomImage> images = new LinkedList<>();
        for (int[] sc :
                new VectorCombinationComputer(strc.getLatticeVectors(), 1.1)
                        .getSupercellCoordinates()) {
            images.add(new AtomImage(strc.getAtom(0), sc));
        }
        List<VoronoiFace> faces = cell.computeFaces(images);

        // Get direct neighbors
        List<VoronoiFace> directFaces = cell.computeDirectNeighbors(faces);

        // Simple tests
        assertEquals(6, directFaces.size());
        assertEquals(images.size() - 6 - 1, faces.size());

        // Make sure direct faces match up with expectations
        List<VoronoiFace> neighborFaces = new LinkedList<>();
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(0), new int[]{1,0,0}), true));
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(0), new int[]{-1,0,0}), true));
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(0), new int[]{0,1,0}), true));
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(0), new int[]{0,-1,0}), true));
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(0), new int[]{0,0,1}), true));
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(0), new int[]{0,0,-1}), true));

        // Test whether they are are all there
        directFaces.removeAll(neighborFaces);
        assertTrue(directFaces.isEmpty());
    }

    @Test
    public void fccTest() throws Exception {
        // Create the cell
        Cell strc = new Cell();
        strc.addAtom(new Atom(new double[]{  0,  0,  0}, 0));
        strc.addAtom(new Atom(new double[]{0.5,0.5,  0}, 0));
        strc.addAtom(new Atom(new double[]{0.5,  0,0.5}, 0));
        strc.addAtom(new Atom(new double[]{  0,0.5,0.5}, 0));

        // Create cell for atom 1
        VoronoiCell cell = new VoronoiCell(strc.getAtom(0), true);

        // Compute faces
        List<AtomImage> images = new LinkedList<>();
        for (int[] sc :
                new VectorCombinationComputer(strc.getLatticeVectors(), 4.0)
                        .getSupercellCoordinates()) {
            images.add(new AtomImage(strc.getAtom(0), sc));
            images.add(new AtomImage(strc.getAtom(1), sc));
            images.add(new AtomImage(strc.getAtom(2), sc));
            images.add(new AtomImage(strc.getAtom(3), sc));
        }
        List<VoronoiFace> faces = cell.computeFaces(images);

        // Get direct neighbors
        List<VoronoiFace> directFaces = cell.computeDirectNeighbors(faces);
        assertTrue(directFaces.get(0).getFaceDistance() <=
                directFaces.get(directFaces.size() - 1).getFaceDistance());

        // Simple tests
        assertEquals(12, directFaces.size());
        assertEquals(images.size() - 12 - 1, faces.size());

        // Make sure direct faces match up with expectations
        List<VoronoiFace> neighborFaces = new LinkedList<>();
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(1), new int[]{0,0,0}),
                true));
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(1), new int[]{0,-1,0}),
                true));
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(1), new int[]{-1,0,0}),
                true));
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(1), new int[]{-1,-1,0}),
                true));
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(2), new int[]{-1,0,0}),
                true));
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(2), new int[]{-1,0,-1}),
                true));
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(2), new int[]{0,0,0}),
                true));
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(2), new int[]{0,0,-1}),
                true));
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(3), new int[]{0,0,0}),
                true));
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(3), new int[]{0,0,-1}),
                true));
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(3), new int[]{0,-1,0}),
                true));
        neighborFaces.add(new VoronoiFace(strc.getAtom(0),
                new AtomImage(strc.getAtom(3), new int[]{0,-1,-1}),
                true));

        // Test whether they are are all there
        directFaces.removeAll(neighborFaces);
        assertTrue(directFaces.isEmpty());
    }

    @Test
    public void intersectionTest() throws Exception {
        // Create the cell
        Cell strc = new Cell();
        strc.addAtom(new Atom(new double[]{0,0,0}, 0));

        // Create cell for atom 1
        VoronoiCell cell = new VoronoiCell(strc.getAtom(0), true);

        // Make sure direct faces match up with expectations
        List<AtomImage> neighborFaces = new LinkedList<>();
        neighborFaces.add(new AtomImage(strc.getAtom(0), new int[]{1,0,0}));
        neighborFaces.add(new AtomImage(strc.getAtom(0), new int[]{-1,0,0}));
        neighborFaces.add(new AtomImage(strc.getAtom(0), new int[]{0,1,0}));
        neighborFaces.add(new AtomImage(strc.getAtom(0), new int[]{0,-1,0}));
        neighborFaces.add(new AtomImage(strc.getAtom(0), new int[]{0,0,2}));
        neighborFaces.add(new AtomImage(strc.getAtom(0), new int[]{0,0,-1}));

        // Assemble cell
        cell.computeCell(neighborFaces);

        // Perform cut
        VoronoiFace cutFace = new VoronoiFace(cell.getAtom(),
                new AtomImage(cell.getAtom(), new int[]{0,0,1}), true);
        cell.computeIntersection(cutFace);

        // Check results
        assertTrue(cutFace.isClosed());
        assertEquals(4, cutFace.NEdges());
        assertTrue(cell.geometryIsValid());
        assertEquals(6, cell.nFaces());
        assertEquals(1.0, cell.getVolume(), 1e-6);
    }

    @Test
    public void testEdgeReplacement() throws Exception {
        // Create a SC cell
        Cell strc = new Cell();
        strc.addAtom(new Atom(new double[]{0,0,0}, 0));

        // Create cell for atom 1
        VoronoiCell cell = new VoronoiCell(strc.getAtom(0), true);

        // Compute faces
        List<AtomImage> images = new LinkedList<>();
        for (int[] sc :
                new VectorCombinationComputer(strc.getLatticeVectors(), 1.1)
                        .getSupercellCoordinates()) {
            images.add(new AtomImage(strc.getAtom(0), sc));
        }
        cell.computeCell(images);

        // Make sure it turned out OK
        assertEquals(1.0, cell.getVolume(), 1e-6);

        // Find position of atom that will take a corner off
        Vector3D cornerA = new Vector3D(0.4, 0.5, 0.5);
        Vector3D cornerB = new Vector3D(0.5, 0.4, 0.5);
        Vector3D cornerC = new Vector3D(0.5, 0.5, 0.4);
        Plane plane = new Plane(cornerA, cornerB, cornerC, 1e-6);
        Vector3D atomPos = (Vector3D) plane.project(Vector3D.ZERO);
        atomPos = atomPos.scalarMultiply(2.0);
        strc.addAtom(new Atom(atomPos.toArray(), 0));

        // Cut off the corner
        cell.computeIntersection(new VoronoiFace(cell.getAtom(),
                new AtomImage(strc.getAtom(1), new int[]{0,0,0}), false));
        double vol = cell.getVolume();
        assertEquals(7, cell.nFaces());

        // Compute a cell that will cut off just slightly more area
        cornerC = new Vector3D(0.5, 0.5, 0.3);
        plane = new Plane(cornerA, cornerB, cornerC, 1e-6);
        atomPos = (Vector3D) plane.project(Vector3D.ZERO);
        atomPos = atomPos.scalarMultiply(2.0);
        strc.addAtom(new Atom(atomPos.toArray(), 0));
        assertTrue(cell.computeIntersection(new VoronoiFace(cell.getAtom(),
                new AtomImage(strc.getAtom(2), new int[]{0,0,0}), false)));
        assertEquals(7, cell.nFaces());
        assertTrue(cell.geometryIsValid());
        assertTrue(cell.getVolume() < vol);
        //new EdgeWriter().writeToFile(cell, "debug/edgeReplaced.edges");
    }

    @Test
    public void testVertexReplacement() throws Exception {
        // Create a SC cell
        Cell strc = new Cell();
        strc.addAtom(new Atom(new double[]{0,0,0}, 0));

        // Create cell for atom 1
        VoronoiCell cell = new VoronoiCell(strc.getAtom(0), true);

        // Compute faces
        List<AtomImage> images = new LinkedList<>();
        for (int[] sc :
                new VectorCombinationComputer(strc.getLatticeVectors(), 1.1)
                        .getSupercellCoordinates()) {
            images.add(new AtomImage(strc.getAtom(0), sc));
        }
        cell.computeCell(images);

        // Make sure it turned out OK
        assertEquals(1.0, cell.getVolume(), 1e-6);

        // Find position of atom that will take a corner off
        Vector3D cornerA = new Vector3D(0.4, 0.5, 0.5);
        Vector3D cornerB = new Vector3D(0.5, 0.4, 0.5);
        Vector3D cornerC = new Vector3D(0.5, 0.5, 0.4);
        Plane plane = new Plane(cornerA, cornerB, cornerC, 1e-6);
        Vector3D atomPos = (Vector3D) plane.project(Vector3D.ZERO);
        atomPos = atomPos.scalarMultiply(2.0);
        strc.addAtom(new Atom(atomPos.toArray(), 0));

        // Cut off the corner
        cell.computeIntersection(new VoronoiFace(cell.getAtom(),
                new AtomImage(strc.getAtom(1), new int[]{0,0,0}), false));
        double vol = cell.getVolume();
        assertEquals(7, cell.nFaces());

        // Compute a cell that will cut off just slightly more area
        cornerA = new Vector3D(0.4, 0.5, 0.5);
        cornerB = new Vector3D(0.5, 0.35, 0.5);
        cornerC = new Vector3D(0.5, 0.5, 0.35);
        plane = new Plane(cornerA, cornerB, cornerC, 1e-6);
        atomPos = (Vector3D) plane.project(Vector3D.ZERO);
        atomPos = atomPos.scalarMultiply(2.0);
        strc.addAtom(new Atom(atomPos.toArray(), 0));
        VoronoiFace newFace = new VoronoiFace(cell.getAtom(),
                new AtomImage(strc.getAtom(2), new int[]{0,0,0}), false);
        assertTrue(cell.computeIntersection(newFace));
        assertEquals(7, cell.nFaces());
        assertTrue(cell.getVolume() < vol);
        assertTrue(cell.geometryIsValid());

        // Remove that face
        cell.removeFace(newFace);
//        new EdgeWriter().writeToFile(cell, "debug/removed.edges");
        assertEquals(6, cell.nFaces());
        assertEquals(6, (int) cell.getPolyhedronShape().get(4));
        assertEquals(1.0, cell.getVolume(), 1e-6);
        assertTrue(cell.geometryIsValid());
    }

    @Test
    public void testExtendedFaces() throws Exception {
        // Create a B1 crystal
        Cell strc = new Cell();
        double[][] basis = new double[3][];
        basis[0] = new double[]{0.0,0.5,0.5};
        basis[1] = new double[]{0.5,0.0,0.5};
        basis[2] = new double[]{0.5,0.5,0.0};
        strc.setBasis(basis);
		Atom atom = new Atom(new double[]{0,0,0}, 0);
        strc.addAtom(atom);
		atom = new Atom(new double[]{0.5,0.5,0.5}, 1);
        strc.addAtom(atom);

        // Compute tessellation
        List<BaseVoronoiCell> cells = VoronoiTessellationCalculator.compute(strc, false);

        // Get the faces of Atom 0
        List<BaseVoronoiFace> faces = cells.get(0).getExtendedFaces(cells, 0);
        int type1count = 0;
        for (BaseVoronoiFace face : faces) {
            if (face.getOutsideAtom().getAtom().getType() == 1) {
                type1count++;
            }
        }
        assertEquals(6, faces.size());
        assertEquals(6, type1count);

        // Get faces of Atom 0 + 1st shell neighbors
        faces = cells.get(0).getExtendedFaces(cells, 1);
        type1count = 0;
        for (BaseVoronoiFace face : faces) {
            if (face.getOutsideAtom().getAtom().getType() == 1) {
                type1count++;
            }
        }
        assertEquals(30, faces.size());
        assertEquals(0, type1count);

        // Get faces of Atom 0 + 2nd shell neighbors
        faces = cells.get(0).getExtendedFaces(cells, 2);
        type1count = 0;
        for (BaseVoronoiFace face : faces) {
            if (face.getOutsideAtom().getAtom().getType() == 1) {
                type1count++;
            }
        }
        assertEquals(6 * 5 + 8 * 3 + 12 * 2, faces.size());
        assertEquals(78, type1count);
    }
}
