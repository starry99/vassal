package vassal.analysis.voronoi.java;

import vassal.analysis.voronoi.java.VoronoiEdge;
import vassal.analysis.voronoi.java.VoronoiFace;
import org.junit.Test;
import static org.junit.Assert.*;
import vassal.data.Atom;
import vassal.data.Cell;
import vassal.data.AtomImage;

/**
 *
 * @author Logan Ward
 */
public class VoronoiVertexTest {

    public VoronoiVertexTest() {
    }

    @Test
    public void testCreation() throws Exception {
        // Make a simple crystal
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 0));

        // Initialize faces
        AtomImage image = new AtomImage(cell.getAtom(0), new int[]{0,0,1});
        VoronoiFace face1 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{0,1,0});
        VoronoiFace face2 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{1,0,0});
        VoronoiFace face3 = new VoronoiFace(cell.getAtom(0), image, true);

        // Create edges
        VoronoiEdge edge1 = new VoronoiEdge(face1, face2);
        VoronoiEdge edge2 = new VoronoiEdge(face1, face3);

        // Create vertex
        VoronoiVertex vertA = new VoronoiVertex(edge1, edge2);

        // Test properties
        assertEquals(edge2, vertA.getPreviousEdge());
        assertEquals(edge1, vertA.getNextEdge());

        // Make sure the order of edges on creation doesn't matter
        assertEquals(vertA, new VoronoiVertex(edge2, edge1));

        // Create a new vertex, ensure that it is different
        VoronoiEdge edge3 = new VoronoiEdge(face2, face3);
        assertFalse(vertA.equals(new VoronoiVertex(edge3, edge2)));
    }

}
