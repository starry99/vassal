package vassal.analysis.voronoi;

import java.util.Collection;
import java.util.Objects;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import vassal.data.Atom;

/**
 * Base class for a vertex in a Voronoi tessellation.
 * @author Logan Ward
 */
public abstract class BaseVoronoiVertex implements Comparable<BaseVoronoiVertex> {

    /**
     * Compute the centroid of a group of vertices
     * @param points Points
     * @return Centroid
     */
    protected static Vector3D getCentroid(Collection<BaseVoronoiVertex> points) {
        double[] centerSum = new double[3];
        for (BaseVoronoiVertex point : points) {
            double[] x = point.getPosition().toArray();
            for (int i = 0; i < x.length; i++) {
                centerSum[i] += x[i];
            }
        }
        for (int i = 0; i < 3; i++) {
            centerSum[i] /= points.size();
        }
        Vector3D centroid = new Vector3D(centerSum);
        return centroid;
    }

    /** Position of vertex */
    protected final Vector3D Position;
    /** Distance from cell center */
    protected final double Distance;

    /**
     * Initialize a Voronoi vertex.
     *
     * <p>Developer note: This class does not check if position is null. This might
     * occur depending on the input to a constructor of a subclass {@linkplain VoronoiVertex}
     *
     * @param insideAtom Atom on inside of cell
     * @param position Position of vertex
     */
    protected BaseVoronoiVertex(Atom insideAtom, Vector3D position) {
        Position = position;
        Distance = Position.distance(new Vector3D(insideAtom.getPositionCartesian()));
    }

    /**
     * Compute distance between vertices
     * @param vertex Other vertex
     * @return Distance between them
     */
    public double distanceFrom(BaseVoronoiVertex vertex) {
        return Position.distance(vertex.Position);
    }

    /**
     * Get the distance from the center of the cell.
     * @return Distance
     */
    public double getDistanceFromCenter() {
        return Distance;
    }

    /**
     * Get the Position of this vertex
     * @return Position of this vertex
     */
    public Vector3D getPosition() {
        return Position;
    }

    @Override
    public String toString() {
        return Position.toString();
    }

    @Override
    public abstract int compareTo(BaseVoronoiVertex other);

    @Override
    public int hashCode() {
        int hash = 3 + Position.hashCode();
        return hash;
    }

}
