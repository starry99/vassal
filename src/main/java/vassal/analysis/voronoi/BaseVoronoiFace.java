package vassal.analysis.voronoi;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.apache.commons.math3.geometry.euclidean.threed.Plane;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import vassal.data.Atom;
import vassal.data.AtomImage;

/**
 * Facet in a Voronoi tessellation
 * @author Logan Ward
 */
public abstract class BaseVoronoiFace implements Comparable<BaseVoronoiFace> {

    /**
     * Get distance from the center to the closest-point on a radical plane dividing
     *  two atoms.
     * @param radius1 Radius of atom #1
     * @param radius2 Radius of atom #2
     * @param d Distance between atoms
     * @return Distance from atom #1 center to plane
     */
    protected static double getPlaneDistance(double radius1, double radius2, double d) {
        return (radius1 * radius1 - radius2 * radius2 + d * d) / 2 / d;
    }
    /** Plane of face */
    protected Plane FacePlane;
    /** Normal of face */
    protected Vector3D FaceNormal;
    /** Edges defining this face */
    protected final List<BaseVoronoiEdge> Edges = new ArrayList<>(5);
    /** Vertices associated with this face */
    protected final List<BaseVoronoiVertex> Vertices = new ArrayList<>(5);
    /** Atom on "inside" of this face */
    protected final Atom InsideAtom;
    /** Atom on the "outside" of this face */
    protected final AtomImage OutsideAtom;
    /** Distance from cell center to face */
    protected double FaceDistance;
    /** Center of the face */
    protected Vector3D FaceCenter;
    /** Face area. Cached result */
    protected double FaceArea = Double.NaN;

    /**
     * Create a blank Voronoi face
     * @param insideAtom Atom "inside" of this face
     * @param outsideAtom Atom on the outside
     * @param radical Whether this face is in a radical plane tessellation
     */
    public BaseVoronoiFace(Atom insideAtom, AtomImage outsideAtom, boolean radical) {
        InsideAtom = insideAtom;
        OutsideAtom = outsideAtom;

        // Compute the rest
        Vector3D insidePos = new Vector3D(InsideAtom.getPositionCartesian());
        Vector3D outsidePos = OutsideAtom.getPosition();
        double atomDist = insidePos.distance(outsidePos);
        if (radical) {
            FaceDistance = getPlaneDistance(InsideAtom.getRadius(),
                    OutsideAtom.getAtom().getRadius(), atomDist);
        } else {
            FaceDistance = atomDist / 2;
        }
        FaceCenter = outsidePos.subtract(insidePos).
                normalize().scalarMultiply(FaceDistance).add(insidePos);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BaseVoronoiFace) {
            BaseVoronoiFace other = (BaseVoronoiFace) obj;
            return other.InsideAtom.getID() == InsideAtom.getID() && other.OutsideAtom.equals(OutsideAtom);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + InsideAtom.hashCode();
        hash = 19 * hash + OutsideAtom.hashCode();
        return hash;
    }

    @Override
    public int compareTo(BaseVoronoiFace o) {
        if (InsideAtom.getID() == o.InsideAtom.getID()) {
            return OutsideAtom.compareTo(o.OutsideAtom);
        } else {
            return Integer.compare(InsideAtom.getID(), o.InsideAtom.getID());
        }
    }

    /**
     * Get the atom on the inside of this face
     * @return Atom
     */
    public Atom getInsideAtom() {
        return InsideAtom;
    }

    /**
     * Get center of this face
     * @return Position of the center of the face
     */
    public Vector3D getFaceCenter() {
        return FaceCenter;
    }

    /**
     * Get identifying information about atom on other side of this face.
     * @return Pair: (Outside Atom ID, Outside Atom Image)
     */
    public AtomImage getOutsideAtom() {
        return OutsideAtom;
    }

    /**
     * Get the plane on which this face lies
     * @return FacePlane
     */
    public Plane getPlane() {
        if (FacePlane == null) {
            FacePlane = new Plane(FaceCenter,
                    OutsideAtom.getPosition()
                            .subtract(new Vector3D(InsideAtom.getPositionCartesian())), 1e-8);
        }
        return FacePlane;
    }

    /**
     * Get normal to this face
     * @return Normal vector (unnormalized)
     */
    public Vector3D getNormal() {
        if (FaceNormal == null) {
            FaceNormal = getPlane().getNormal();
        }
        return FaceNormal;
    }

    /**
     * Get the number of edges this face has
     * @return Number of edges
     */
    public int NEdges() {
        return Edges.size();
    }

    /**
     * Determine the surface area of this face
     * @return Surface area
     */
    public double getArea() {
        // If needed, compute area
        if (Double.isNaN(FaceArea)) {
            // Get centroid of face
            Vector3D centroid = getCentroid();
            // Loop over all edges
            double area = 0;
            for (int i = 0; i < Vertices.size(); i++) {
                BaseVoronoiVertex thisVertex = Vertices.get(i);
                BaseVoronoiVertex nextVertex = Vertices.get((i + 1) % Vertices.size());
                Vector3D a = thisVertex.getPosition().subtract(centroid);
                Vector3D b = nextVertex.getPosition().subtract(centroid);
                area += a.crossProduct(b).getNorm();
            }
            FaceArea = area / 2;
        }
        return FaceArea;
    }

    /**
     * Get the centroid of this face
     * @return Coordinates of centroid
     */
    public Vector3D getCentroid() {
        return BaseVoronoiVertex.getCentroid(new LinkedList<>(Vertices));
    }

    /**
     * Get distance from center of the cell to this face.
     * @return Distance
     */
    public double getFaceDistance() {
        return FaceDistance;
    }

    /**
     * Get distance between central atom and neighbor associated with this face
     * @return Distance in Cartesian units
     */
    public double getNeighborDistance() {
        return Vector3D.distance(new Vector3D(InsideAtom.getPositionCartesian()), OutsideAtom.getPosition());
    }

    /**
     * Get the list of vertices describing this face.
     * @return List of vertices
     */
    public List<BaseVoronoiVertex> getVertices() {
        return new ArrayList<>(Vertices);
    }

    /**
     * Compute number of vertices
     * @return Number of vertices
     */
    public int nVertices() {
        return Vertices.size();
    }

    /**
     * Get vertex iterator. Starts at one vertex and goes over each other vertex
     *  in order
     * @return Read-only iterator over all vertices in face
     */
    public Iterator<BaseVoronoiVertex> getVertexIterator() {
        return new Iterator<BaseVoronoiVertex>() {
            int pos = 0;

            @Override
            public boolean hasNext() {
                return pos < Vertices.size();
            }

            @Override
            public BaseVoronoiVertex next() {
                return Vertices.get(pos++);
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("This is a read-only iterator");
            }
        };
    }

    /**
     * Get the edges that form this face
     * @return List of edges
     */
    public List<BaseVoronoiEdge> getEdges() {
        return new ArrayList<>(Edges);
    }

    /**
     * Get all faces that share a vertex with this face
     * @return Set of all neighboring faces
     */
    public Set<BaseVoronoiFace> getNeighboringFaces() {
        Set<BaseVoronoiFace> output = new TreeSet<>();
        for (BaseVoronoiEdge edge : Edges) {
            output.add(edge.getIntersectingFace());
        }
        return output;
    }

    /**
     * Get number of common vertices between two faces
     * @param otherFace Other face
     * @return Set of common vertices
     */
    public Set<BaseVoronoiVertex> getCommonVertices(BaseVoronoiFace otherFace) {
        Set<BaseVoronoiVertex> output = new TreeSet<>(Vertices);
        output.retainAll(otherFace.Vertices);
        return output;
    }

    /**
     * Get position of point with respect to face.
     * @param point Point in question
     * @return -1 if inside of face (i.e. closer to cell center), 0 if on
     *  the face, 1 if outside of the face
     */
    public int positionRelativeToFace(Vector3D point) {
        Plane facePlane = getPlane();
        if (facePlane.contains(point)) {
            return 0;
        }
        double offset = facePlane.getOffset(point);
        if (offset > 0) {
            return 1;
        } else {
            return -1;
        }
    }

    @Override
    public String toString() {
        return Integer.toString(InsideAtom.getID()) + "->" + OutsideAtom.toString();
    }

    /**
     * Test whether the edges form a closed face.
     * @return Answer
     */
    public abstract boolean isClosed();
}
