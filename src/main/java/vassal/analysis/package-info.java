
/**
 * Classes that perform different kinds of calculations about a structure.
 */
package vassal.analysis;
