package vassal.analysis;

import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.tuple.Pair;
import vassal.data.AtomImage;

/**
 * Compute Atomic Property Weighted Radial Distribution Function (AP-RDF).
 *
 * <p>Follows the work by <a href="http://pubs.acs.org/doi/abs/10.1021/jp404287t">
 * Fernandez <i>et al.</i></a>.
 *
 * <p>Here, we use a scaling factor equal to: <code>1 / &lt;# atoms&gt;</code>
 *
 * @author Logan Ward
 */
public class APRDFAnalysis extends BaseAnalysis {
    /**
     * Cutoff distance used when computing radial distribution function.
     */
    protected double CutoffDistance = 10;
    /**
     * Number of points of the RDF to sample.
     */
    protected int NWindows = 10;
    /**
     * Tool used to compute pair distances
     */
    protected PairDistanceAnalysis DistanceComputer;
    /**
     * Smoothing factor in PRDF expression
     */
    protected double B = 2;
    /**
     * Accuracy factor. Determines the number of atoms farther than the cutoff
     * radius that are included in the computation of the PRDF. This is necessary
     * due to the Gaussian cutoff term in the RDF.
     */
    protected double AccuracyFactor = 1e-3;


    @Override
    protected void precompute() throws Exception {
        // Determine the maximum distance at which atoms contribute to the PRDF
        //   This is the maximum distance (r) at which exp(-B * (r - R)^2) > AccuracyFactor
        double maxPairDistance = Math.sqrt(-1 * Math.log(AccuracyFactor) / B) + CutoffDistance;

        // Set up the PRDF computer
        DistanceComputer = new PairDistanceAnalysis();
        DistanceComputer.setCutoffDistance(maxPairDistance);
        DistanceComputer.analyzeStructure(Structure);
    }

    /**
     * Set smoothing factor used when computing PRDF.
     * @param B Smoothing factor
     */
    public void setSmoothingFactor(double B) {
        if (B <= 0) {
            throw new IllegalArgumentException("B must be positive. Supplied: " + B);
        }
        this.B = B;
    }

    /**
     * Set the cutoff distance for radial distribution function
     * @param cutoffDistance Desired cutoff distance
     */
    public void setCutoffDistance(double cutoffDistance) {
        if (cutoffDistance <= 0) {
            throw new IllegalArgumentException("Cutoff distance must be positive. Supplied" + cutoffDistance);
        }

        // Set the cutoff distance
        this.CutoffDistance = cutoffDistance;

        // Redo precomputation, if needed
        if (Structure != null) {
            try {
                precompute();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Set the number of points at which to evaluate AP-RDF
     * @param n Desired number of windows
     */
    public void setNWindows(int n) {
        this.NWindows = n;
    }

    /**
     * Compute the AP-RDF of this structure.
     * @param properties Properties of each atom type
     * @return AP-RDF at R at {@linkplain #NWindows} steps between
     * {@linkplain #CutoffDistance} / {@linkplain #NWindows}
     * to {@linkplain #CutoffDistance}, inclusive.
     */
    public double[] computeAPRDF(double[] properties) {
        // Make sure the number of properties is correct
        if (properties.length != Structure.nTypes()) {
            throw new RuntimeException(String.format("Incorrect number of properties."
                    + " Supplied: %d. Required %d", properties.length, Structure.nTypes()));

        }

        // Get the evaluation distances
        double[] evalR = getEvaluationDistances();

        // Initialize output
        double[] apRDF = new double[evalR.length];
        Arrays.fill(apRDF, 0);

        // Loop through each pair of atoms
        for (int i=0; i<Structure.nAtoms(); i++) {
            for (int j=i; j<Structure.nAtoms(); j++) {
                // All images of j within cutoff radius of i
                List<Pair<AtomImage,Double>> images = DistanceComputer.findAllImages(i,j);

                // If i != j, the contributions get added twice
                double timesAdded = i == j ? 1 : 2;

                // Get types of i and j
                int iType = Structure.getAtom(i).getType(),
                        jType = Structure.getAtom(j).getType();

                // Add contributes from each pair
                for (Pair<AtomImage,Double> image : images) {
                    // Evaluate contribution at each distance
                    for (int r=0; r<NWindows; r++) {
                        apRDF[r] += timesAdded
                                * properties[iType]
                                * properties[jType]
                                * Math.exp(-1 * B * Math.pow(image.getRight() - evalR[r], 2));
                    }
                }
            }
        }


        // Scale output by number of atoms squared
        double f = 1.0 / Structure.nAtoms();
        for (int r=0; r<apRDF.length; r++) {
            apRDF[r] *= f;
        }

        return apRDF;
    }

    /**
     * Get the distances at which the PRDF should be analyzed
     * @return List of distances
     */
    public double[] getEvaluationDistances() {
        // Initilize output array
        double[] output = new double[NWindows];

        // Compute the step size
        double stepSize = CutoffDistance / NWindows;

        // Fill the output array
        for (int i=0; i<NWindows; i++) {
            output[i] = (i + 1) * stepSize;
        }

        return output;
    }

}
