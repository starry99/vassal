package vassal.util;

/**
 * Mathematical utility functions
 * @author Logan Ward
 */
public class MathUtils {
    /**
     * Return a vector of with spaced according to the same multiplicative factor.
     *
     * <p>Example: logspace(1, 100, 3) => [1, 10, 100]</p>
     * @param min Minimum value
     * @param max Maximum value
     * @param number
     * @return Array of evenly spaced numbers where [0] == min, arranged in ascending order
     */
    public static double[] logspace(double min, double max, int number) {
        // Check input
        if (number < 0) {
            throw new IllegalArgumentException("number must be >= 1");
        }
        if (min >= max) {
            throw new IllegalArgumentException("minimum must be > maximum");
        }
        if (min <= 0 || max <= 0) {
            throw new IllegalArgumentException("mimimum and maximum must be greater than zero");
        }

        // Compute the logspace
        double logMin = Math.log(min), logMax = Math.log(max);
        double[] output = linspace(logMin, logMax, number);
        for (int n=0; n < number; n++) {
            output[n] = Math.exp(output[n]);
        }
        return output;
    }

    /**
     * Generate a list of evenly-spaced numbers
     *
     * <p>Example: linspace(0, 5, 6) => [0,1,2,3,4,5]</p>
     * @param min Minimum number
     * @param max Maximum number
     * @param number Size of list to generate
     * @return List of evenly-spaced numbers
     */
    private static double[] linspace(double min, double max, int number) {
        // Check inputs
        if (number < 0) {
            throw new IllegalArgumentException("number must be >= 1");
        }
        if (min >= max) {
            throw new IllegalArgumentException("minimum must be > maximum");
        }

        // Check output
        double stepSize = number > 1 ? (max- min) / (number - 1) : 0;
        double[] output = new double[number];
        for (int n=0; n < number; n++) {
            output[n] = min + stepSize * n;
        }
        return output;
    }
}
