
package vassal.analysis;

import java.util.List;
import vassal.data.Atom;
import vassal.data.Cell;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import vassal.data.AtomImage;

/**
 * @author Logan Ward
 */
public class PairDistanceAnalysisTest {
    private Cell Structure = new Cell();

    @Before
    public void generateStructure() throws Exception {
        Structure.setBasis(new double[]{1,1,1}, new double[]{90,90,90});
        Structure.addAtom(new Atom(new double[]{0,0,0}, 0));

    }

    @Test
    public void testGetAllNeighborsOfAtom() throws Exception {
        PairDistanceAnalysis x = new PairDistanceAnalysis();

        // With orthorhombic basis
        x.analyzeStructure(Structure);
        x.setCutoffDistance(1.1);
        List<Pair<AtomImage,Double>> output = x.getAllNeighborsOfAtom(0);
        assertEquals(6, output.size());
        assertEquals(1.0, output.get(0).getValue(), 1e-6);

        // Adding a second atom
        Structure.addAtom(new Atom(new double[]{0.5,0.5,0.5}, 0));
        output = x.getAllNeighborsOfAtom(0);
        assertEquals(14, output.size());

        // Altering the basis to something wierd
        double[][] newBasis = Structure.getBasis();
        newBasis[1][0] = 14;
        output = x.getAllNeighborsOfAtom(0);
        assertEquals(14, output.size());

        // Check that images match up
        Vector3D centerPos = new Vector3D(Structure.getAtom(0).getPositionCartesian());
        for (Pair<AtomImage,Double> image : output) {
            assertEquals(image.getValue(),
                    image.getKey().getPosition().subtract(centerPos).getNorm(),
                    1e-6);
        }
    }

    @Test
    public void testPRDF() throws Exception {
        PairDistanceAnalysis x = new PairDistanceAnalysis();

        // With orthorhombic basis
        x.analyzeStructure(Structure);
        x.setCutoffDistance(2.1);

        // Run code
        double[][][] prdf = x.computePRDF(50);
        assertEquals(1, prdf.length);
        assertEquals(1, prdf[0].length);
        assertEquals(50, prdf[0][0].length);

        // Make sure that it finds 4 peaks
        int nPeaks = 0;
        for (double val : prdf[0][0]) {
            if (val > 0) {
                nPeaks++;
            }
        }
        assertEquals(4, nPeaks);

        // Add another atom, repeat
        Structure.addAtom(new Atom(new double[]{0.5,0.5,0.5}, 1));

        // Run again
        prdf = x.computePRDF(50);
        assertEquals(2, prdf.length);
        assertEquals(2, prdf[0].length);
        assertEquals(50, prdf[0][0].length);

        // Make sure A-B prdf has 2 peaks
        nPeaks = 0;
        for (double val : prdf[0][1]) {
            if (val > 0) {
                nPeaks++;
            }
        }
        assertEquals(2, nPeaks);

        // Increase basis
        Structure.setBasis(new double[]{2,2,2}, new double[]{90,90,90});
        x.analyzeStructure(Structure);

        // Run again
        prdf = x.computePRDF(50);
        assertEquals(2, prdf.length);
        assertEquals(2, prdf[0].length);
        assertEquals(50, prdf[0][0].length);

        // Make sure A-B prdf has 1 peak
        nPeaks = 0;
        for (double val : prdf[0][1]) {
            if (val > 0) {
                nPeaks++;
            }
        }
        assertEquals(1, nPeaks);
    }

}
