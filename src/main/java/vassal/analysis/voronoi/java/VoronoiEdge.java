package vassal.analysis.voronoi.java;

import java.util.*;
import org.apache.commons.math3.geometry.euclidean.threed.*;
import vassal.analysis.voronoi.BaseVoronoiEdge;

/**
 * Line formed by two faces on a VoronoiDiagram
 * @author Logan Ward
 */
public class VoronoiEdge extends BaseVoronoiEdge {

    /**
     * Generate a edge on a VoronoiFace. Ensures that edge is CCW with respect
     * to the face on which it resides.
     * @param face Plane defining one face containing this edge
     * @param intersectingFace Plane defining the other face defining this edge
     * @throws java.lang.Exception If faces are parallel
     */
    public VoronoiEdge(VoronoiFace face, VoronoiFace intersectingFace) throws Exception {
        super(face, intersectingFace);
    }

    /**
     * Find the edge that is likely to be "next" on a face that contains
     * this edge. This is computed by first computing all edges that are oriented
     * CCW to this edge. Next, the edge that is closest to the origin is found.
     * If multiple edges intersect at the same point, the one with the greatest
     * between the direction of this edge is selected.
     * @param candidates All candidate edges
     * @return The next edge. Null if no suitable candidate is found
     */
    public VoronoiEdge findNextEdge(List<BaseVoronoiEdge> candidates) {
        // Locate the CCW edges
        List<VoronoiEdge> ccwEdges = new ArrayList<>(candidates.size());
        for (BaseVoronoiEdge edge : candidates) {
            if (this.isCCW(edge)) {
                ccwEdges.add((VoronoiEdge) edge);
            }
        }

        // Check if any were found
        if (ccwEdges.isEmpty()) {
            return null;
        } else if (ccwEdges.size() == 1) {
            return ccwEdges.get(0);
        }

        // Find the closest edge(s)
        List<VoronoiEdge> closestEdges = new LinkedList<>();
        double minDist = Double.POSITIVE_INFINITY;
        Vector3D minPoint = Vector3D.POSITIVE_INFINITY;
        for (VoronoiEdge edge : ccwEdges) {
            // If this line contains the minimum point, add it to list
            Line otherLine = edge.getLine();
            if (otherLine.contains(minPoint)) {
                closestEdges.add(edge);
                continue;
            }

            // Compute intersection point
            Vector3D intersection = Line.intersection(otherLine);
            if (intersection == null) {
                continue; // Line is antiparallel
            }

            // See if its the closest
            double x = Line.getAbscissa(intersection);
            if (x < minDist) {
                closestEdges.clear();
                minDist = x;
                minPoint = intersection;
                closestEdges.add(edge);
            }
        }

        // If only one edge, return answer
        if (closestEdges.size() == 1) {
            return closestEdges.get(0);
        }

        // Otherwise, find the edge with the largest angle
        double maxAngle = 0;
        VoronoiEdge choice = null;
        for (VoronoiEdge edge : closestEdges) {
            double angle = Vector3D.angle(Line.getDirection(),
                    edge.getLine().getDirection());
            if (angle > maxAngle) {
                choice = edge;
                maxAngle = angle;
            }
        }
        return choice;
    }

    /**
     * Get the vertex at the beginning of this vector
     * @return Starting vertex
     * @throws java.lang.Exception
     */
    @Override
    public VoronoiVertex getStartVertex() throws Exception {
        return new VoronoiVertex(this, (VoronoiEdge) PreviousEdge);
    }

    /**
     * Get the vertex at the end of this vector
     * @return Ending vertex
     * @throws java.lang.Exception
     */
    @Override
    public VoronoiVertex getEndVertex() throws Exception {
        return new VoronoiVertex(this, (VoronoiEdge) NextEdge);
    }

    @Override
    public VoronoiEdge generatePair() {
        try {
            return new VoronoiEdge((VoronoiFace) getIntersectingFace(),
                    (VoronoiFace) getEdgeFace());
        } catch (Exception e) {
            throw new RuntimeException("Shouldn't be possible.");
        }
    }

    @Override
    public VoronoiEdge getNextEdge() {
        return (VoronoiEdge) super.getNextEdge();
    }

    @Override
    public BaseVoronoiEdge getPreviousEdge() {
        return (VoronoiEdge) super.getPreviousEdge();
    }
}
